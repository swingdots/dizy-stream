var Twitter = require('twitter');
const conf = require('./twitter.conf.json')

 
var client = new Twitter(conf.api_key);


const user_timeline = function(screen_name, callback){
    var params =  {screen_name: screen_name};
    client.get('statuses/user_timeline', params, callback);
}

const timeline = function(callback){
    var params =  {screen_name: conf.screen_name};
    client.get('statuses/user_timeline', params, callback);
}

const update = function(status, callback){
    var params = {status: status}
    client.post('statuses/update', params, callback);
}

const destroy = function (status, callback) {
    client.post('statuses/destroy/' + status, callback);
}


module.exports = {
    client: client,
    timeline: timeline,
    update: update,
    destroy: destroy
};