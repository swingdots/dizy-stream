const amqp = require("amqplib/callback_api");
const eventHandler = require("./event-bus");

var url = "";
var amqpConn = null;
var pubExchange = "";
var conExchange = "";
var pubChannel = null;
var conChannel = null;
var exchangeType = "";
var pubKey = "";
var conKeys = null;
var eventTag = "";

/***********************************************************************
* initializes variables for rabbit connections
*
* url : amqp address (i.e. amqp://localhost)
* pubExchange : exchange name for publisher exchange
* conExchange : exchange name for consumer exchange
* pubKey : topic key string for publisher
* conKeys : topic key strings for consumer
* eTag : event tag string for onMessage
* eHandler : eventHandler object
***********************************************************************/
function init(url, pubEx, conEx, cKeys, eType, eTag) {
  url = url;
  pubExchange = pubEx;
  conExchange = conEx;
  conKeys = cKeys;
  exchangeType = eType;
  eventTag = eTag;
}


/***********************************************************************
* establishes amqp connection
* upon sucess, starts publisher & consumer
***********************************************************************/
function startRabbit() {
    amqp.connect(url, function(err, conn) {
        if (err) {
            console.error("[DIZYHUB:AMQP]", err.message);
            return setTimeout(startRabbit, 1000);
        }
        conn.on("error", function(err) {
            if (err.message !== "Connection closing") {
                console.error("[DIZYHUB:AMQP] conn error", err.message);
            }
        });
        conn.on("close", function() {
            console.error("[DIZYHUB:AMQP] reconnecting");
            return setTimeout(startRabbit, 1000);
        });

        console.log("[DIZYHUB:AMQP] connected");
        amqpConn = conn;

        onConnect();
    });
}


function onConnect() {
  startPublisher();
  startConsumer();
}


function startPublisher() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[DIZYHUB:AMQP] channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[DIZYHUB:AMQP] channel closed")
    });
    ch.assertExchange(pubExchange, exchangeType, {durable: false});
    pubChannel = ch;
    console.log("[DIZYHUB:AMQP] channel open");
  });
}

/***********************************************************************
* publishes msg to amqp
*
* key : publish routing_key string
* message : msg string to publish
***********************************************************************/
function publish(key, message) {
  pubKey = key;

  try {
    pubChannel.publish(pubExchange, pubKey, message, { persistent: true }, function(err, ok) {
      if (err) {
        console.error("[DIZYHUB:AMQP] conn closed after trying publishing", err);
        pubChannel.connection.close();
      }
    });
  } catch (e) {
    console.error("[DIZYHUB:AMQP] publish", e.message);
  }
}


function startConsumer() {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on("error", function(err) {
      console.error("[DIZYHUB:AMQP] channel error", err.message);
    });
    ch.on("close", function() {
      console.log("[DIZYHUB:AMQP] channel closed")
    });
    ch.assertExchange(conExchange, exchangeType, {durable: false});
    conChannel = ch;
    console.log("[DIZYHUB:AMQP] channel open");
    conChannel.assertQueue('', {exclusive: true}, function(err, q) {
      console.log(' [*] Waiting for logs. To exit press CTRL+C');

      conKeys.forEach(function(key) {
        conChannel.bindQueue(q.queue, conExchange, key);
      });

      conChannel.consume(q.queue, function(msg) {
        console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
        if(!eventHandler)
          console.log("[DIZYHUB:AMQP] EventHandler is undefined");
        else
          eventHandler.emit(eventTag, msg)
        }, {noAck: true});
    });
  });
}


function closeOnErr(err) {
    if (!err) return false;
    console.error("[DIZYHUB:AMQP] error", err);
    amqpConn.close();
    return true;
}

function getPubExchange() {
  return pubExchange;
}

function getPubKey() {
  return pubKey;
}

function getConExchange() {
  return conExchange;
}

function getConKeys() {
  return conKey;
}

function setConKeys(conKeys) {
  conKeys = conKeys;
}

module.exports.init = init;
module.exports.start = startRabbit;
module.exports.publish = publish;
module.exports.getPubExchange = getPubExchange;
module.exports.getPubKey = getPubKey;
module.exports.getConExchange = getConExchange;
module.exports.getConKeys = getConKeys;
module.exports.setConKeys = setConKeys;