const express = require('express');
const router = express.Router();
const url = require('url');

const conf = require('../chat.conf.json')

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
const COLLECTION_CHAT = "chat"
const COLLECTION_COUNT = "count"

const client = new MongoClient(conf.mongo_url, conf.mongo_options);
var db = undefined;
const useMongo = false;

const handleError = (err) => {
    console.error(`${err.message} error`);
    console.error(err.stack);
}

// Connect
const connection = (closure) => {

    if (!conf.use_mongo)
        return closure({message: "mongo disabled"});

    if (db !== undefined)
        return closure(null, db);

    client.connect(function (err) {
        // assert.equal(null, err);
        if(err)
            return closure({message: "db error"});
        db = client.db(conf.mongo_db);
        closure(null, db);
    });
};

const close = () => {
    // mclient.close();
    mclient = undefined;
    db = undefined;
}

// Error handling
const sendError = (err, res, code) => {
    if(code === undefined)
        code = 500;
    var response = {
        message: typeof err == 'object' ? err.message : err
    }
    res.status(code).json(response);
};

router.get('/', (req, res) => {
    var cnt = 100;
    if(req.query.cnt){
        cnt = parseInt(req.query.cnt);
    }
    connection((err, db) => {
        if(err){
            return sendError(err, res);
        }
        db.collection(COLLECTION_CHAT)
            .find()
            .sort({$natural:-1}).limit(cnt)
            .toArray().then((users) => {
                var response = {data: users}
                res.json(response);
            })
            .catch((err) => {
                close();
                sendError(err, res);
            });
    });
});

router.get('/count', (req, res) => {
    connection((err, db) => {
        if(err){
            return sendError(err, res);
        }
        db.collection(COLLECTION_COUNT)
            .find()
            .sort({count:-1, word:1}).limit(5)
            .toArray().then((users) => {
                var response = {data: users}
                res.json(response);
            })
            .catch((err) => {
                close();
                sendError(err, res);
            });
    });
});

router.post('/', (req, res) => {
    
    if(req.body.message === undefined || req.body.name === undefined){
        sendError('bad request', res);
        return;
    }
    
    var data = {
        time: new Date(),
        name: req.body.name,
        message: req.body.message
    };

    connection((err, db) => {
        if(err){
            return sendError(err, res);
        }
        db.collection(COLLECTION_CHAT)
            .insertOne(data)
            .then((users) => {
                var response = {data: users}
                res.json(response);
            })
            .catch((err) => {
                close();
                sendError(err, res);
            });
    });
});

router.delete('/', (req, res) => {    
    sendError('not implemented', res, 501);
});

module.exports = router;