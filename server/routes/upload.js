const express = require('express')
const router = express.Router();
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })
// var upload = multer().single('avatar')
var lastPath = 'public/assets/image_not_uploaded.png';
var filename = 'image_not_uploaded.png';

router.post('/image', upload.single('image-png'), function (req, res) {
    if(req.file !== undefined){
        res.send('Uploaded! : ' + req.file);
        lastPath = req.file.path;
        filename = req.file.filename + '.png'
    }
    else {
        res.status(400);
        res.json({message: 'no image'});
    }
});


router.get('/image', (req, res) => {
    if (lastPath){
        res.download(lastPath, filename);
    }
    else{
        // res.sendFile('public/assets/image_not_uploaded.png', {root: '.'});
        res.status(400);
        res.json({message: 'no file'});
    }
});

module.exports = router;
