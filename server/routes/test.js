const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
    var response = ["test", "api"];
    res.status(200);
    res.json(response);
});


/////////////////////////////////////
// rabbitmq relay api
var amqp = require('amqplib/callback_api');
var pub_exchange = 'ai'
var channel = undefined;

amqp.connect('amqp://localhost', function(err, conn) {
  conn.createChannel(function(err, ch) {
    // ch.assertExchange(pub_exchange, 'topic', {durable: false});
    channel = ch;
  });

//   setTimeout(function() { conn.close(); process.exit(0) }, 500);
});

router.post('/amqp/:routing_key', (req, res) => {
    var routing_key = req.params.routing_key;
    // console.log(routing_key);
    // var pub_routing_key = 'ai.play.etg'
    var msg =  JSON.stringify(req.body);
    channel.publish(pub_exchange, routing_key, new Buffer(msg));
    res.status(200);
    res.json({status: "ok"});
});


/////////////////////////////////////



module.exports = router;
