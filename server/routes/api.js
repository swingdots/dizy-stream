const express = require('express');
const path = require('path')


const app = express();
const router = express.Router();

app.use('/chat', require('./chat'))
app.use('/upload', require('./upload'))
app.use('/test', require('./test'))

module.exports = app;
