const tmi = require('tmi.js')
const conf = require('./twitch.conf.json')
const http = require('http');
const eventHandler = require("./event-bus");
const util = require("./util-chat.js");
const eventTag = "chat";
var wcUpdated = false;

const options = {
    options: {
        debug: true
    },
    connection: {
        cluster: "aws", reconnect: true
    },
    identity: {
        username: conf.username, password: conf.password
    },
    channels: [conf.channel]
    
}

var client = new tmi.client(options);
client.connect();

// const express = require('express');

// const options = {
//     options: {
//         debug: true
//     },
//     connection: {
//         cluster: "aws", reconnect: true
//     },
//     identity: {
//         username: conf.username, password: conf.password
//     },
//     channels: [conf.channel]
// }
// var client = new tmi.client(options);
// client.connect();

// const MongoClient = require('mongodb').MongoClient;
// const ObjectID = require('mongodb').ObjectID;
// const assert = require('assert');
// const COLLECTION_CHAT = "chat"
// const COLLECTION_COUNT = "count"

// var db = undefined;


const handleError = (err) => {
    console.error(`${err.code} error`);
    console.error(err.stack);
}

// Connect
// const connection = (closure) => {
//     if(db !== undefined)
//         return closure(db);
//     MongoClient.connect(conf.mongo_url, conf.mongo_options, function (err, client) {
//         // assert.equal(null, err);
//         if(err !== null){
//             console.error(err.message);
//             return;
//         }
//         db = client.db(conf.mongo_db);
//         closure(db);
//     });
// };


// const getWordCount = (callback) => {
//     connection((db) => {
//         db.collection(COLLECTION_COUNT)
//             .find()
//             .sort({count:-1, word:1}).limit(5)
//             .toArray().then((wc) => {
//                 callback(wc, null);
//             })
//             .catch((err) => {
//                 callback(null, err)
//             });
//         });
// }

// const insertMessage = (name, message, words, callback) => {
//     var date = new Date()
//     var data = {
//         time: date.valueOf(),
//         name: name,
//         message: message
//     };

//     connection((db) => {
//         // insert message
//         db.collection(COLLECTION_CHAT).insert(data)
//             .then((r) => {
//             })
//             .catch((err) => {
//                 callback(err);
//             });

//         // word count
//         words.forEach( (element, index, array) => {
//             db.collection(COLLECTION_COUNT).update({word:element}, { $inc: { count: 1 }}, {upsert: true})
//                 .then((r) =>{
//                     wcUpdated = true;
//                 })
//                 .catch((err) => {
//                     callback(err);
//                 });
//         })
        
//     });
// }



const setOnChat = function(callback) {
    client.on("chat", function (channel, userstate, message, self) {
        if (self || conf.username == userstate.username) {
            return;
        }

        var objMessage = util.messageToObject(message, userstate.username, 'twitch');
        if (objMessage){

            switch (objMessage.type) {
                case 'cmd':
                    callback(objMessage);
                    break;
                case 'ans':
                    client.say(conf.channel, objMessage.message);
                    break;
                case 'chat':
                    eventHandler.emit(eventTag, userstate.username, message);
                    util.insertMessage(userstate.username, message, (err, res) => {
                        if(err){
                            handleError(err)
                            return;
                        }
                        wcUpdated = res;
                    });
                    callback(objMessage);
                    break;
            }
        }
        
        // console.log("[Message received] "+ userstate['display-name'] + ": " + message);
        // var us = {
        //     badges: { broadcaster: '1' },
        //     color: null,
        //     'display-name': 'hc9shin',
        //     emotes: null,
        //     id: 'a4785d59-20dc-4ad1-828b-8c76fbc12a82',
        //     mod: false,
        //     'room-id': '172263494',
        //     subscriber: false,
        //     'tmi-sent-ts': '1525598626190',
        //     turbo: false,
        //     'user-id': '172263494',
        //     'user-type': null,
        //     'emotes-raw': null,
        //     'badges-raw': 'broadcaster/1',
        //     username: 'hc9shin',
        //     'message-type': 'chat'
        // }
    });
}

const setOnWordCount = function (callback){
    wcUpdated = true;
    setInterval(() => {
        if (!wcUpdated)
            return;
        wcUpdated = false;
        util.getWordCount((err, res) => {
            if(err){
                handleError(err);
                return;
            }

            callback(res);
        });
    }, 1000);
}

client.on("connected", function (address, port) {
    console.log("Address: "+ address + ", Port: " + port);
    // client.say(conf.channel, "Hello. I'm connected...");
    var t = new Date();
    client.say(conf.channel, "[" + t.toLocaleString() + "] Chat-bot connected. ");
});

  
module.exports = {
    client: client,
    setOnChat: setOnChat,
    setOnWordCount: setOnWordCount
};