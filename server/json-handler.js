/************************************************************************* 
* json schema compliance tester and json generator
*************************************************************************/

const moment = require("moment");
const Ajv = require('ajv');
const ajv = new Ajv({allErrors: true});

// json schemas
const adminServerConf = require("./json-schema/admin.server.json");
const adminGameConf = require("./json-schema/admin.game.json");
const adminLogConf = require("./json-schema/admin.log.json");
const channelChatConf = require("./json-schema/channel.chat.json");
const channelDonationConf = require("./json-schema/channel.donation.json");
const channelSubscribeConf = require("./json-schema/channel.subscribe.json");
const gameHSConf = require("./json-schema/game.hs.json");
const logConf = require("./json-schema/log.json");
const sessionMultipleChoiceConf = require("./json-schema/session.multiple_choice.json");
const sessionShortAnswerConf = require("./json-schema/session.short_answer.json");


/*********************************************************************************************************
{
    "description":"class:admin topic:server - to control server module",
    "type":"object",
    "required":["class", "topic", "cmd", "datetime"],
    "properties":{
        "class":{"const":"admin"},
        "topic":{"const":"server"},
        "cmd":{"enum":["load_config", "shutdown", "broadcast", "reset"]},
        "option":{"type" : "object"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getAdminServerObj() {
    var adminServerObj = {
        class:"admin",
        topic:"server",
        cmd:"reset",
        option:{},
        datetime:getCurrentUTC()
    };
    return adminServerObj;
}



/*********************************************************************************************************
{
    "description":"class:admin topic:game - to control game module",
    "type":"object",
    "required":["class", "topic", "cmd", "datetime"],
    "properties":{
        "class":{"const":"admin"},
        "topic":{"const":"game"},
        "cmd":{"enum":["load_model", "load_game", "load_config", "set_action", "shutdown", "broadcast", "reset"]},
        "option":{"type" : "object"},
        "datetime":{"format" : "date-time"}
    }
}
*********************************************************************************************************/

function getAdminGameObj() {
    var adminGameObj = {
        class:"admin",
        topic:"game",
        cmd:"reset",
        option:{},
        datetime:getCurrentUTC()
    };
    return adminGameObj;
}



/*********************************************************************************************************
{
    "description":"class:admin topic:log - to control or set up logging level",
    "type":"object",
    "required":["class", "topic", "level", "datetime"],
    "properties":{
        "class":{"const":"admin"},
        "topic":{"const":"log"},
        "level":{"enum":["error", "info", "debug"]},
        "datetime":{"format" : "date-time"}
    }
}
*********************************************************************************************************/

function getAdminLogObj() {
    var adminLogObj = {
        class:"admin",
        topic:"log",
        level:"info",
        origin:"server",
        msg:"",
        datetime:getCurrentUTC()
    };
    return adminLogObj;
}



/*********************************************************************************************************
{
    "description":"class:channel topic:chat - chat or comment event in channel",
    "type":"object",
    "required":["class", "topic", "msg", "platform", "datetime"],
    "properties":{
        "class":{"const":"channel"},
        "topic":{"const":"chat"},
        "direction":{"enum":["in", "out"]},
        "channel":{"type" : "string"},
        "user":{"type" : "string"},
        "msg":{"type" : "string"},
        "platform":{"enum":["twitch", "youtube", "twitter"]},
        "viewer_count":{"type":"integer"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getChannelChatObj() {
    var channelChatObj = {
        class:"channel",
        topic:"chat",
        direction:"in",
        channel:"",
        user:"",
        msg:"",
        platform:"twitch",
        viewer_count:0,
        datetime:getCurrentUTC()
    };
    return channelChatObj;
}


/*********************************************************************************************************
{
    "description":"class:channel topic:donation - donation event in channel",
    "type":"object",
    "required":["class", "topic", "user", "msg", "amount", "unit", "platform", "datetime"],
    "properties":{
        "class":{"const":"channel"},
        "topic":{"const":"donation"},
        "channel":{"type":"string"},
        "user":{"type":"string"},
        "msg":{"type":"string"},
        "amount":{"type":"number"},
        "unit":{"type":"string"},
        "platform":{"enum":["twitch", "youtube", "twitter"]},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getChannelDonationObj() {
    var channelDonationObj = {
        class: "channel",
        topic: "donation",
        channel: "",
        user: "",
        msg: "",
        amount: 0,
        unit: "won",
        platform: "twitch",
        datetime: getCurrentUTC()
    };
    return channelDonationObj;
}



/*********************************************************************************************************
{
    "description":"class:channel topic:subscribe - subscribe event in channel",
    "type":"object",
    "required":["class", "topic", "channel", "user", "platform", "datetime"],
    "properties":{
        "class":{"const":"channel"},
        "topic":{"const":"subscribe"},
        "channel":{"type":"string"},
        "user":{"type":"string"},
        "subscribe_count":{"type":"integer"},
        "platform":{"enum":["twitch", "youtube", "twitter"]},
        "datetime":{"format" : "date-time"}
    }
}
*********************************************************************************************************/

function getChannelSubscribeObj() {
    var channelSubscribeObj = {
        class: "channel",
        topic: "subscribe",
        channel: "",
        user: "",
        subscribe_count: 0,
        platform: "twitch",
        datetime: getCurrentUTC()
    };
    return channelSubscribeObj;
}



/*********************************************************************************************************
{
    "description" : "class:game topic:hc - for hearthstone game",
    "type":"object",
    "required":["class", "topic", "datetime"],
    "properties":{
        "class":{"const": "game"},
        "topic":{"const" : "hs"},
        "my_class":{"enum":["druid", "hunter", "mage", "paladin", "priest", "rogue", "shaman", "warlock", "warrior"]},
        "opponent_class":{"enum":["druid", "hunter", "mage", "paladin", "priest", "rogue", "shaman", "warlock", "warrior"]},
        "my_cards":{"type":"array","items":{"type":"string"}},
        "opponent_cards":{"type":"array","items":{"type":"string"}},
        "turn":{"type":"integer"},
        "my_minions":{"type":"array","items":{"type":"string"}},
        "opponent_minions":{"type":"array","items":{"type":"string"}},
        "status":{"enum":["win", "loss", "playing", "standby"]},
        "win_probability":{"type" : "number", "minimum" : 0, "maximum" : 1.0},
        "cmd":{"enum":[ "play_on", "set_action"]},
        "option":{"type":"object"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getGameHSObj() {
    var gameHSObj = {
        class:"game",
        topic:"hs",
        my_class:"druid",
        opponent_class:"druic",
        my_cards:[],
        opponent_cards:[],
        turn:0,
        my_minions:[],
        opponent_minions:[],
        status:"standby",
        win_probability:0,
        cmd:"play",
        option:{},
        datetime:getCurrentUTC()
    };
    return gameHSObj;
}


/*********************************************************************************************************
{
    "description":"class:admin topic:log - to propagate logging information",
    "type":"object",
    "required":["class", "topic", "level", "origin", "msg", "datetime"],
    "properties":{
        "class":{"const":"log"},
        "topic":{"const":"log"},
        "level":{"enum":["error", "info", "debug"]},
        "origin":{"enum":["game", "server"]},
        "msg":{"type":"string"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getLogObj() {
    var logObj = {
        class: "log",
        topic: "log",
        level: "info",
        origin: "",
        msg: "",
        datetime: getCurrentUTC()
    };
    return logObj;
}



/*********************************************************************************************************
{
    "description":"class:session topic:multiple_choice",
    "type":"object",
    "required":["class", "topic", "options", "status", "datetime"],
    "properties":{
        "class":{"const":"session"},
        "topic":{"const":"multiple_choice"},
        "options":{"type":"array", "items": {"type" : ["string", "number"]}},
        "channel":{"type":"string"},
        "option_type":{"enum":["string", "integer", "number"]},
        "messages":{
            "type":"array",
            "items":{
                "type":"object",
                "properties":{"user":{"type":"string"}, "msg":{"type":["string", "number"]}}}},
        "result":{"type":"string"},
        "owner":{"type":"string"},
        "status":{"enum":["on", "off", "cancel", "request"]},
        "duration":{"type":"integer"},
        "num_response":{"type":"integer"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getSessionMultipleChoiceObj() {
    var sessionMultipleChoiceObj = {
        class:"session", 
        topic:"multiple_choice",
        options:[], 
        channel:"",
        option_type:"string",
        messsages :[], 
        result:"",
        owner:"",
        status:"off",
        duration:0,
        num_response:0,
        datetime:getCurrentUTC()
    };
    return sessionMultipleChoiceObj;
}



/*********************************************************************************************************
{
    "description":"class:session topic:short_answer",
    "type":"object",
    "required":["class", "topic", "options", "status", "datetime"],
    "properties":{
        "class":{"const":"session"},
        "topic":{"const":"short_answer"},
        "channel":{"type":"string"},
        "messages":{
            "type":"array",
            "items":{
                "type":"object",
                "properties":{"user":{"type":"string"}, "msg":{"type":["string", "number"]}}}},
        "result":{"type":["string", "number"]},
        "owner":{"type":"string"},
        "status":{"enum":["on", "off", "cancel", "request"]},
        "duration":{"type":"integer"},
        "num_response":{"type":"integer"},
        "datetime":{"format":"date-time"}
    }
}
*********************************************************************************************************/

function getSessionShortAnswerObj() {
    var sessionShortAnswerObj = {
        class:"session",
        topic:"shoutout",
        channel:"",
        messages:[],
        result:"",
        owner:"",
        status:"request",
        duration:0,
        num_response:0,
        datetime:getCurrentUTC()
    };
    return sessionShortAnswerObj;
}



function validateAdminServer(data) {
    var validate = ajv.compile(adminServerConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateAdminGame(data) {
    var validate = ajv.compile(adminGameConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}



function validateAdminLog(data) {
    var validate = ajv.compile(adminLogConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateChannelChat(data) {
    var validate = ajv.compile(channelChatConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateChannelDonation(data) {
    var validate = ajv.compile(channelDonationConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateChannelSubscribe(data) {
    var validate = ajv.compile(channelSubscribeConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateGameHS(data) {
    var validate = ajv.compile(gameHSConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateLog(data) {
    var validate = ajv.compile(logConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}

function validateSessionMultipleChoice(data) {
    var validate = ajv.compile(sessionMultipleChoiceConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function validateSessionShortAnswer(data) {
    var validate = ajv.compile(sessionShortAnswerConf);
    var valid = validate(data);
    if (valid) return true;
    else return false;
}


function getCurrentUTC() {
    return moment.utc().format();
}
  


module.exports = {
    getAdminServerObj: getAdminServerObj,
    getAdminGameObj: getAdminGameObj,
    getAdminLogObj: getAdminLogObj,
    getGameHSObj: getGameHSObj,
    getLogObj: getLogObj,
    getChannelChatObj: getChannelChatObj,
    getChannelDonationObj: getChannelDonationObj,
    getChannelSubscribeObj: getChannelSubscribeObj,
    getSessionShortAnswerObj: getSessionShortAnswerObj,
    getSessionMultipleChoiceObj: getSessionMultipleChoiceObj,
    validateAdminServer: validateAdminServer,
    validateAdminGame: validateAdminGame,
    validateAdminLog: validateAdminLog,
    validateGameHS: validateGameHS,
    validateLog: validateLog,
    validateChannelChat: validateChannelChat,
    validateChannelDonation: validateChannelDonation,
    validateChannelSubscribe: validateChannelSubscribe,
    validateSessionShortAnswer: validateSessionShortAnswer,
    validateSessionMultipleChoice: validateSessionMultipleChoice

};
