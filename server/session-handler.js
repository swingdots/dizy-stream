
const eventHandler = require("./event-bus");
const jsonHandler = require("./json-handler");
const eventConf = require("./event.conf.json");
//const programConf = require("");

// event tag
const adminEvent = eventConf.adminEvent;
const channelChatEvent = eventConf.channelChatEvent;   // chat received or to send
const channelSubscribeEvent = eventConf.channelSubscribeEvent;
const channelDonationEvent = eventConf.channelDonationEvent;
const sessionEvent = eventConf.sessionEvent;   // shot event received
const sessionRequestEvent = eventConf.sessionRequest;
const sessionReadyEvent = eventConf.sessionReady;
const sessionOnEvent = eventConf.sessionOn;
const sessionOffEvent = eventConf.sessionOff;
const sessionCancelEvent = eventConf.sessionCancel;

// session control variables
var sessionType = "";
var sessionDuration = 0;
var sessionObj = null;
var theTimer = null;
var sessionMessages = [];


//////////////////////////////////////////////////
var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({ port: 40510 });
let list_ws = [];


wss.on('connection', function connection(ws) {
    console.log("[pp] ws client connected");
    list_ws.push(ws);
    ws.send(JSON.stringify({ ws_status: 'connected' }));

    //ws new message
    ws.on('message', function incoming(message) {
        console.log('[pp] received: %s', message);
        // enforce every message to be json format
        var msgObj = JSON.parse(message);
        // new session request
        if (msgObj.status === 'request') {
            eventHandler.emit(sessionRequestEvent, msgObj);
        }
        else if(msgObj.status ==='cancel') {
            eventHandler.emit(sessionCancelEvent, msgObj);
        }
        //var sobj = jsonHandler.getSessionRequestObj();
        //eventHandler.emit(sessionReqeustEvent, sobj);
    })

    // ws close
    ws.on('close', function close() {
        console.log("[pp] ws client disconnected");
        index = list_ws.indexOf(ws);
        if (index >= 0) {
            list_ws.splice(index, 1);
        }
    });
})



/**********************************************************************************************
 Event Handlers
**********************************************************************************************/

// on admin event
eventHandler.on(adminEvent, function (obj) {
});

// on channel chat event
eventHandler.on(channelChatEvent, function (user, msg) {
    if (sessionObj !== null && sessionObj.status === "on") {
        if (sessionType === "voting") {
            var chatObj = { user: user, msg: msg };
            sessionMessages.push(chatObj);
        }
    }
});

eventHandler.on(sessionOnEvent, function (obj) {
    console.log('[pp] session ON >>' + JSON.stringify(obj));
    list_ws.forEach((ws, index, arr) => {
        ws.send(JSON.stringify(obj));
    });
});

eventHandler.on(sessionOffEvent, function (obj) {
    console.log('[pp] session OFF >>' + JSON.stringify(obj));
    list_ws.forEach((ws, index, arr) => {
        ws.send(JSON.stringify(obj));
    });
});

eventHandler.on(sessionRequestEvent, function (obj) {
    console.log('[pp] sessionRequst event' + JSON.stringify(obj));
    handleSessionRequest(obj);
});


eventHandler.on(sessionCancelEvent, function (obj) {
    console.log('[pp] sessionCancel event' + JSON.stringify(obj));
    handleSessionCancel(obj);
});


/**********************************************************************************************
 Core Handlers
**********************************************************************************************/

function init() {
    initializeVariables();
}

function handleSessionRequest(obj) {
    if (obj.status === 'request') {
        if (sessionObj !== null) {
            console.log('[pp] another session in progress. cannot start new session');
            return;
        }
        if (obj.topic === "voting") {
            sessionObj = jsonHandler.getSessionVotingObj();
            sessionObj.channel = obj.channel;
            sessionObj.owner = obj.owner;
            sessionType = obj.topic;
            sessionRequestStatus = obj.status;
            sessionDuration = obj.duration;

            startSession(sessionObj);
        }
        else if(obj.topic==='shoutout') {
            sessionObj = jsonHandler.getSessionShoutoutObj();
            sessionObj.channel = obj.channel;
            sessionObj.owner = obj.owner;
            sessionType = obj.topic;
            sessionRequestStatus = obj.status;
            sessionDuration = obj.duration;

            startSession(sessionObj);
        }
    }
}

function handleSessionCancel(obj) {
    if (obj.status === 'cancel') {
        if (sessionObj === null) {
            console.log('[pp] no session in progress. cannot cancel');
            return;
        }
        endSession();
    }
}


function startSession() {
    sessionObj.status = "on";
    if (sessionDuration > 0)
        theTimer = setTimeout(endSession, sessionDuration);
    eventHandler.emit(sessionOnEvent, sessionObj);
}

function endSession() {
    sessionObj.status = "off";
    eventHandler.emit(sessionOffEvent, sessionObj);
    setTimeout(function () {
        initializeVariables();
    }, 1000);
}

function initializeVariables() {
    sessionType = "";
    sessionDuration = 0;
    sessionObj = null;
    theTimer = null;
    sessionMessages = [];
}



/**********************************************************************************************
 Helper Functions
**********************************************************************************************/

// checks if msg is an integer number
function isIntegerNumber(msg) {
    // check if number
    if (!isNaN(msg)) {
        var temp = msg * 1;
        // check if integer
        if ((temp) % 1 == 0) {
            return true;
        }
    }
    return false;
}



module.exports = {
    init: init
};
