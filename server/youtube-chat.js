const { google } = require('googleapis');
const cs = require('./youtube.conf.json')
const eventHandler = require("./event-bus");
const util = require("./util-chat.js");
const eventTag = "chat";
var wcUpdated = false;


const oauth2Client = new google.auth.OAuth2(cs.client_id, cs.client_secret)

// oauth2Client.on('tokens', (tokens) => {
//     if (tokens.refresh_token) {
//         // store the refresh_token in my database!
//         console.log(tokens.refresh_token);
//     }
//     // console.log(tokens.access_token);
//     console.log("access token refreshed");
// });

oauth2Client.setCredentials({ refresh_token: cs.refresh_token });
google.options({ auth: oauth2Client });

const youtube = google.youtube({ version: 'v3' });

var pageToken = undefined;
var timeLast = undefined;

var pollingInterval = 3000;



const handleError = (err) => {
    console.error(`${err.code} error`);
    console.error(err.stack);
}


const handleMessage = (item, callback) => {
    var message = item.snippet.displayMessage;
    var authorChannelId = item.snippet.authorChannelId;
    var objMessage = util.messageToObject(message, authorChannelId, 'youtube');
    if (objMessage) {
        switch (objMessage.type) {
            case 'cmd':
                callback(objMessage);
                break;
            case 'ans':
                // client.say(conf.channel, objMessage.message);
                insertLiveChat(liveChatId, objMessage.message);
                break;
            case 'chat':
                eventHandler.emit(eventTag, authorChannelId, message);
                util.insertMessage(authorChannelId, message, (err, res) => {
                    if (err) {
                        handleError(err)
                        return;
                    }
                    wcUpdated = res;
                });
                callback(objMessage);
                break;
        }
    }
}

const initLiveChat = (liveChatId, callback) => {
    youtube.liveChatMessages.list({ liveChatId: liveChatId, part: 'id' }, (err, res) => {
        if (err) {
            handleError(err);
            return;
        }
        pageToken = res.data.nextPageToken;
        pollingInterval = res.data.pollingIntervalMillis;
        callback(res.data);
    });
}

const insertLiveChat = (liveChatId, message) => {

    var options = {
        part: 'snippet',
        requestBody: {
            snippet: {
                liveChatId: liveChatId,
                type: "textMessageEvent",
                textMessageDetails: { "messageText": message }
            }
        }
    };

    youtube.liveChatMessages.insert(options, (err, res) => {
        if (err) {
            handleError(err);
            return;
        }
        // console.log(res);
    });
}


const getLiveChat = (liveChatId, callback) => {

    var option = { liveChatId: liveChatId, part: 'snippet, authorDetails', pageToken: pageToken };

    youtube.liveChatMessages.list(option, (err, res) => {
        if (err) {
            handleError(err);
            return;
        }

        pageToken = res.data.nextPageToken;
        // pollingInterval = res.data.pollingIntervalMillis;
        if (pollingInterval !== res.data.pollingIntervalMillis) {
            console.log(`[pollingInterval changed] ${pollingInterval} to ${res.data.pollingIntervalMillis}`)
        }
        if (res.data.items.length > 0) {
            res.data.items.forEach(element => {
                handleMessage(element, callback);
                console.log("[YouTube chat message] ", element.snippet.displayMessage);
            });
        }

    });
}



const setOnChat = function (callback) {

    // params = { broadcastStatus: 'active', broadcastType: 'all', part: 'snippet' };
    params = { broadcastStatus: 'all', broadcastType: 'all', part: 'snippet' };
    youtube.liveBroadcasts.list(params, (err, res) => {
        if (err) {
            handleError(err);
            return;
        }

        var liveChatId = undefined;
        for (let index = 0; index < res.data.items.length; index++) {
            const element = res.data.items[index];
            if (element.snippet.hasOwnProperty('liveChatId')) {
                liveChatId = element.snippet.liveChatId;
                break;
            }
        }

        if (liveChatId === undefined) {
            // console.log("off line");
            console.error("There is no activated live chat");
            return;
        }
        // const liveChatId = res.data.items[0].snippet.liveChatId;
        console.log("[YouTube] liveChatId:", liveChatId);

        initLiveChat(liveChatId, (data) => {
            console.log("[YouTube] pollingIntervalMillis:", data.pollingIntervalMillis);
            setInterval(getLiveChat, data.pollingIntervalMillis, liveChatId, callback);
        });

        // youtube.liveChatMessages.list({ liveChatId: liveChatId, part: 'snippet' }, (err, res) => {
        //     if (err) {
        //         handleError(err);
        //         return;
        //     }
        //     console.log(`${res.data.items.length} messages`);
        //     res.data.items.forEach(element => {
        //         console.log(element.snippet.displayMessage);
        //     });
        // });
    });


}


const setOnWordCount = function (callback) {
    wcUpdated = true;
    setInterval(() => {
        if (!wcUpdated)
            return;
        wcUpdated = false;
        util.getWordCount((err, res) => {
            if (err) {
                handleError(err);
                return;
            }

            callback(res);
        });
    }, 1000);
}


async function getLiveChatAsync(callback) {

    try {

        var dt = new Date() - timeLast;
        if (dt < pollingInterval) {
            return;
        }

        var res = undefined;
        res = await youtube.liveChatMessages.list({ liveChatId: liveChatId, part: 'snippet, authorDetails', pageToken: pageToken });
        pageToken = res.data.nextPageToken;
        pollingInterval = res.data.pollingIntervalMillis;
        timeLast = new Date(res.headers.date);

        if (res.data.items.length > 0) {
            res.data.items.forEach(element => {
                handleMessage(element, callback);
                console.log("[YouTube chat message] ", element.snippet.displayMessage);
            });
        }

    }
    catch (e) {
        // console.error(e.stack);
        console.error(e.message);
        timeLast = new Date();
        pollingInterval = 5000;
    }

}


async function setOnChatAsync(callback) {
    var res = undefined;

    // request liveBroadcasts
    res = await youtube.liveBroadcasts.list({ broadcastStatus: 'all', broadcastType: 'all', part: 'snippet' });
    if (res.status != 200) {
        console.error(res.status, res.statusText);
        return;
    }

    // get liveChatId
    for (let index = 0; index < res.data.items.length; index++) {
        const element = res.data.items[index];
        if (element.snippet.hasOwnProperty('liveChatId')) {
            liveChatId = element.snippet.liveChatId;
            break;
        }
    }
    if (liveChatId === undefined) {
        // console.log("off line");
        console.error("There is no activated live chat");
        return;
    }
    console.log("[YouTube] liveChatId:", liveChatId);

    // request liveChatMessages
    res = await youtube.liveChatMessages.list({ liveChatId: liveChatId, part: 'id' });
    pageToken = res.data.nextPageToken;
    timeLast = new Date(res.headers.date);

    setInterval(getLiveChatAsync, 1000, callback);

}


module.exports = {
    client: youtube,
    setOnChat: setOnChatAsync,
    setOnWordCount: setOnWordCount
};