/************************************************************************* 
* event handler object 
*************************************************************************/
const EventEmitter = require('events');
const eventHandler = new EventEmitter();

eventHandler.on('uncaughtException', function (err) {
    console.error(err);
});

module.exports = eventHandler;