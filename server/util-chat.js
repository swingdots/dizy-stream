const conf = require('./chat.conf.json')



const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
const COLLECTION_CHAT = "chat"
const COLLECTION_COUNT = "count"

const client = new MongoClient(conf.mongo_url, conf.mongo_options);
var db = undefined;


var listManagerCommand = [];

conf.commands_manager.forEach((el, index, array) => {
    var pattern = new RegExp('^' + el.cmd.replace('.', '\\.').replace('*', '\\w*') + '$');
    listManagerCommand.push(pattern);
});



const connection = (closure) => {
    if (!conf.use_mongo)
    return ;

    if (db !== undefined)
        return closure(db);

    client.connect(function (err) {
        // assert.equal(null, err);
        if(err)
            return;
        db = client.db(conf.mongo_db);
        closure(db);
    });
};


const getWordCount = (callback) => {
    connection((db) => {
        db.collection(COLLECTION_COUNT)
            .find()
            .sort({ count: -1, word: 1 }).limit(5)
            .toArray().then((wc) => {
                callback(null, wc);
            })
            .catch((err) => {
                callback(err, null)
            });
    });
}

const insertMessage = (name, message, callback) => {
    var words = message.split(/\s{1,}/).filter(String);
    var date = new Date()
    var data = {
        time: date.valueOf(),
        name: name,
        message: message
    };

    connection((db) => {
        // insert message
        db.collection(COLLECTION_CHAT).insertOne(data)
            .then((r) => {
            })
            .catch((err) => {
                callback(err);
            });

        // word count
        words.forEach((element, index, array) => {
            db.collection(COLLECTION_COUNT).updateOne({ word: element }, { $inc: { count: 1 } }, { upsert: true })
                .then((r) => {
                    // wcUpdated = true;
                    callback(null, true);
                })
                .catch((err) => {
                    callback(err, null);
                });
        })

    });
}



function handleManagerCommand(user, message) {
    if (conf.managers.find((el) => { return el == user; })) {
        return listManagerCommand.find(function (element) {
            return element.exec(message) != null;
        });
    }
    return undefined
}

function handleCommand(message) {
    return conf.commands.find(function (element) {
        return element.cmd == message;
    });

}


const messageToObject = (message, username, from) => {
    var obj = undefined;
    if (message[0] === "!") {
        // commands

        var splt = message.match(/[\w가-힣.!-]+|'(?:\\'|[^'])*'/g);
        // var splt = message.match(/[\w.!]+|"(?:\\"|[^"])+"/g);

        var cmd = splt[0].slice(1);
        var ans_manager = handleManagerCommand(username, cmd);
        if (ans_manager) {
            obj = { type: "cmd", from: from, cmd: splt };
        }
        var ans = handleCommand(cmd);
        if (ans) {
            obj = { type: "ans", from: from, message: ans.message };
        }
    }
    else {
        // chat
        obj = { type: 'chat', from: from, message: message };

    }

    return obj;
}



module.exports = {
    messageToObject: messageToObject,
    getWordCount: getWordCount,
    insertMessage: insertMessage
}