const eventHandler = require("./event-bus");
const rabbit = require("./rabbit");
const jsonHandler = require("./json-handler");
const rabbitConf = require("./rabbit.conf.json");
const eventConf = require("./event.conf.json");


// event tag
const adminEvent = eventConf.adminEvent;
const channelChatEvent = eventConf.channelChatEvent;   // chat received or to send
const channelSubscribeEvent = eventConf.channelSubscribeEvent;
const channelDonationEvent = eventConf.channelDonationEvent;
const sessionEvent = eventConf.sessionEvent;   // shot event received
const msgEvent = eventConf.msgEvent; // rabbit message received

// rabbit variables
const url = rabbitConf.url + "?heartbeat=" + rabbitConf.heartbeat;
const pubExchange = rabbitConf.pubExchange;
const conExchange = rabbitConf.conExchange;
const pubKey = rabbitConf.pubKey;
const conKeys = rabbitConf.conKeys;
const exchangeType = rabbitConf.exchangeType;


// message class
const adminClass = "admin";
const gameClass = "game";
const channelClass = "channel";
const sessionClass = "session";

// message topic
const adminGameTopic = "game";
const adminServerTopic = "server";
const adminLogTopic = "log";
const gameHSTopic = "hs";
const channelChatTopic = "chat";
const channelSubscribeTopic = "subscribe";
const channelDonationTopic = "donation";
const sessionVotingTopic = "voting";

// msg direction
const inbound = "in";
const outbound = "out";

// control
var isPrintable = true; //console.log print control
var numChats = 0;
var theTimer = null;

/**********************************************************************************************
 Initialization
**********************************************************************************************/

// initialize rabbit variables
rabbit.init(url, pubExchange, conExchange, conKeys, exchangeType, msgEvent);


// rabbit message handler
eventHandler.on(msgEvent, function (msg) {
    var inbound_obj = JSON.parse(msg.content.toString());
    onClass(inbound_obj);
});


/**********************************************************************************************
 Twitch channel handler
**********************************************************************************************/

// twitch chat event handler
eventHandler.on(channelChatEvent, function (user, msg) {
    onTwitchChat(user, msg);
});

// donation event handler
eventHandler.on(channelDonationEvent, function (user, msg) {
    onTwitchDonation(user, msg);
});

// subscribe event handler
eventHandler.on(channelSubscribeEvent, function (user, msg) {
    onTwitchSubscribe(user, msg);
});


// twitch chat received event
function onTwitchChat(user, msg) {
    // send chat to rabbit
    var outbound_obj = jsonHandler.getChannelChatObj();
    outbound_obj.user = user;
    outbound_obj.msg = msg;
    outbound_obj.platform = "twitch";
    sendMessage(onTwitchChat, channelChatTopic, outbound_obj);
}


// twitch donation event
function onTwitchDonation(user, msg) {
}


// twitch subscribe event
function onTwitchSubscribe(user, msg) {
}



/**********************************************************************************************
 Message Handlers
**********************************************************************************************/

function onClass(obj) {
    if (obj.topic === adminClass) {
        onAdminClass(obj);
    }
    else if (obj.topic === gameClass) {
        onGameClass(obj);
    }
    else if (obj.topic === channelClass) {
        onChannelClass(obj);
    }
    else if (obj.topic === sessionClass) {
        onSessionClass(obj);
    }
}


function onAdminClass(obj) {
    eventHandler.emit(adminEvent, obj);
}

// hearthstone related operations to be determined later
function onGameClass(obj) {
    if (obj.topic === gameHSTopic) {
        console.log("[rabbit] game hearthstone msg received");
    }
}

// currently no modules send channel messages to the server
function onChannelClass(obj) {
    if (obj.topic === channelChatTopic) {
        console.log("[rabbit] channel chat msg received");
    }
}

// currently no modules send shot messages to the server
function onSessionClass(obj) {
    if (obj.topic === sessionVotingTopic) { 
        console.log("[rabbit] session voting msg received");
    }
}


/**********************************************************************************************
 Helper Functions
**********************************************************************************************/

function onPrint(origin, dest, msg) {
    if (isPrintable) {
        if (dest === inbound) console.log(origin.name + " : " + inbound + " : " + msg);
        else if (dest === outbound) console.log(origin.name + " : " + outbound + " : " + msg);
    }
}


function sendMessage(origin, send_object) {
    var out_json = JSON.stringify(send_object);
    onPrint(origin, outbound, out_json);
    rabbit.publish(pubKey, new Buffer(out_json));
}




module.exports = rabbit;

