const WebSocket = require('ws');
const http = require('http');
const url = require('url');

// const wss = new WebSocket.Server({
//   port: 8080,
//   perMessageDeflate: {
//     zlibDeflateOptions: { // See zlib defaults.
//       chunkSize: 1024,
//       memLevel: 7,
//       level: 3,
//     },
//     zlibInflateOptions: {
//       chunkSize: 10 * 1024
//     },
//     // Other options settable:
//     clientNoContextTakeover: true, // Defaults to negotiated value.
//     serverNoContextTakeover: true, // Defaults to negotiated value.
//     clientMaxWindowBits: 10,       // Defaults to negotiated value.
//     serverMaxWindowBits: 10,       // Defaults to negotiated value.
//     // Below options specified as default values.
//     concurrencyLimit: 10,          // Limits zlib concurrency for perf.
//     threshold: 1024,               // Size (in bytes) below which messages
//                                    // should not be compressed.
//   }
// });

const wssStream = new WebSocket.Server({ noServer: true });
const wssTest = new WebSocket.Server({ noServer: true });
const server = http.createServer().listen(8080);

server.on('upgrade', (request, socket, head) => {
  const pathname = url.parse(request.url).pathname;
  // console.log(pathname);
  if (pathname === '/stream') {
    wssStream.handleUpgrade(request, socket, head, (ws) => {
      wssStream.emit('connection', ws);
    });
  } else if (pathname === '/test') {
    wssTest.handleUpgrade(request, socket, head, (ws) => {
      wssTest.emit('connection', ws);
    });
  } else {
    socket.destroy();
  }
});

wssTest.on('connection', function connection(ws) {
  console.log("[/test] WebSocket client connected...");
  ws.on('message', function incoming(message) {
    console.log('[/test] received: %s', message);
    if(message == 'ping'){
      ws.send("pong");
    }
  });
  ws.on('close', function close() {
    console.log("[/test] WebSocket client disconnected...");
  });
});
// wss.on('connection', function connection(ws) {
//   console.log(ws)
//   // ws.on('message', function incoming(message) {
//   //   console.log('received: %s', message);
//   //   ws.send("{message:'hello'}");
//   // });
// });

module.exports = {
  stream: wssStream,
  test: wssTest
};