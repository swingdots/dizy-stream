// var canvas = document.getElementById('canvas');
// var ctx = canvas.getContext('2d');
// ctx.font = "10px Arial";
// ctx.fillStyle = 'white';
// ctx.strokeStyle = 'black';

var canvas = null;
var ctx = null;
var queue = [];
var cnt = 0;

const randInt = (max) => {
    return Math.floor(Math.random() * max);
}

const render = () => {
    // add message
    // var x = randInt(canvas.width);
    // var y = randInt(canvas.height);
    // var msg = "message " + cnt++;
    // var obj = { message: "message " + cnt, x: x, y: y };
    // queue.push(obj);
    // if(queue.length > 10){
    //     queue.shift();
    // }
    
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    queue.forEach( (element, index, array) => {
        ctx.strokeText(element.message, element.x, element.y);
        ctx.fillText(element.message, element.x, element.y);
    })
}

const pushMessage = (message) => {
    var x = randInt(canvas.width - 50);
    var y = randInt(canvas.height);
    var obj = { message: message, x: x, y: y };
    queue.push(obj);
    if(queue.length > 10){
        queue.shift();
    }
}

var chatClient = new chatClient({
    channel: '#hc9shin',
    username: 'hc9shin', 
    password: 'oauth:9ev7q8th2jf3wk41kbdrug6jd5hisv',
});

chatClient.onMessage = function onMessage(message){
    if(message !== null){
        var parsed = this.parseMessage(message.data);
        console.log(parsed);
        if(parsed !== null){
            if(parsed.command === "PRIVMSG") {

                if(parsed.username === 'hc9test1') return;

                pushMessage(parsed.message);
                render();

            } else if(parsed.command === "PING") {
                this.webSocket.send("PONG :" + parsed.message);
            }
        }
    }
};

const getWordsCount = () => {

    $.ajax({
        url: '/api/chat/count',
        type: 'GET',
        success: function(data){ 
            for (var i = 0; i < 5; i++) {
                var obj = { word: '----', count: '-' };
                if (data.data.length > i) {
                    obj = data.data[i];
                }
                $('#w' + i).text(obj.word);
                $('#c' + i).text(obj.count);
            }
            setTimeout(getWordsCount, 1000);
        },
        error: function(data) {
            setTimeout(getWordsCount, 3000);
        }
    });

}

$( document ).ready(function() {
    canvas = document.getElementById('canvas');
    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    ctx = canvas.getContext('2d');
    ctx.font = "25px Impact";
    ctx.fillStyle = 'green';
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 3;
    ctx.lineCap="round";

    setTimeout(getWordsCount, 1000);
    // setInterval(getWordsCount, 1000);

    // console.log(chatClient);
    window.chatClient.open();

});
