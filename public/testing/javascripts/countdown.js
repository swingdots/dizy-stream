const countdown = document.querySelector('.countdown');

// Set Launch Date (ms)
//const launchDate = new Date('Jan 1, 2019 13:00:00').getTime();
const target = 10;
var distance = target;
// Update every second
const intvl = setInterval(() => {
    // Get todays date and time (ms)
    //const now = new Date().getTime();

    // Distance from now and the launch date (ms)
    //const distance = launchDate - now;

    // Time calculation
    //const seconds = 10;
    //const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    const seconds = distance;

    // Display result
    countdown.innerHTML = `<div>${seconds}<span>Seconds</span></div>`;
    distance = distance - 1;

    // If launch date is reached
    if (distance < 0) {
        // Stop countdown
        clearInterval(intvl);
        // Style and output text
        countdown.style.color = '#17a2b8';
        countdown.innerHTML = '!!!';
    }
}, 1000);
