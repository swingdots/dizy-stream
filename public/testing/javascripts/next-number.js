const dogIdle = document.querySelector(".dog-idle");
const dogJump = document.querySelector(".dog-jump");
const dogRun = document.querySelector(".dog-run");


/************************************************************************************** 
 * web socket
*/
var ws = new WebSocket('ws://localhost:40510');

// event emmited when connected
ws.onopen = function () {
    console.log('websocket is connected ...')

    // sending a send event to websocket server
    ws.send(JSON.stringify({ws_status:'connected'}));
}

// event emmited when receiving message 
ws.onmessage = function (ev) {
    console.log(ev);
}


var clock = new FlipClock($('.clock'), 10, {
    clockFace: 'Counter',
    autoStart: false,
});
var timer = new FlipClock.Timer(clock, {
    callbacks: {
        interval: function () {
            var time = clock.getTime().time;
            console.log(time);
            if (time == 0) {
                console.log("0 has reached");
                timer.stop();
                dogJumpAnimation();
                var obj = {class: "session", topic: "request", category: "voting", duration: 0, channel: "", status: "request", owner: "", datetime: "2018-09-10T07:45:10Z" };
                ws.send(JSON.stringify(obj));
            }
            clock.decrement()
        },
        start: function() {
            console.log("started");
            dogRunAnimation();
        },
        stop: function() {
            console.log("stopped");
        },
        reset: function () {
            console.log("reset");
        }
    }
});



/************************************************************************************** 
 * animation controller
*/

function dogIdleAnimation() {
    dogIdle.style.display = "block";
    dogRun.style.display = "none";
    dogJump.style.display = "none";
}

function dogRunAnimation() {
    dogIdle.style.display = "none";
    dogRun.style.display = "block";
    dogJump.style.display = "none";
}

function dogJumpAnimation() {
    dogIdle.style.display = "none";
    dogRun.style.display = "none";
    dogJump.style.display = "block";
}



$(document).ready(function () {
    dogIdleAnimation();

    // Attach a click event to a button a decrement the clock
    $('.decrement').click(function () {
        timer.stop();
        clock.setValue(10);
        timer.start();
    });
    $('.reset').click(function () {
        timer.stop();
        clock.reset();
    });
});
