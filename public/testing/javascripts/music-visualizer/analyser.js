define(['util'], function (util) {

var audio = util.getById('music'),
    audioContext = new AudioContext(),
    analyser = audioContext.createAnalyser(),
    audioSource = audioContext.createMediaElementSource(audio);

analyser.fftSize = 512;
var freqByteData = new Uint8Array(analyser.frequencyBinCount);

audioSource.connect(analyser);
analyser.connect(audioContext.destination);

function getData() {
    analyser.getByteFrequencyData(freqByteData);
    return freqByteData;
}

function getFftSize() {
    return analyser.fftSize;
}

return {
    getData: getData,
    getFftSize: getFftSize
}

});
