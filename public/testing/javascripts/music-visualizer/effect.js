define(['util', 'e0', 'e1', 'e2', 'e3', 'e4', 'e5',
    'e6', 'e7', 'e8', 'e9'], function (util) {

var canvas = util.getById('visual-canvas'),
    $effectList = $('#effect-list'),
    effectListHtml = '',
    currentEffect = 0,
    effect = null,
    effects = [],
    i = 0,
    interval = 30,
    isStopped = true,
    len = 0, 
    requestAnimationFrame = window.requestAnimationFrame,
    cancelAnimationFrame = window.cancelAnimationFrame,
    timer = null;

if (localStorage.effect) {
    currentEffect = +localStorage.effect;
}

function beginDraw() {
    isStopped = false;
    if (!effect) {
        setCurrentEffect(currentEffect);
    }
    draw();
}

function saveSetting() {
    localStorage.effect = currentEffect;
}

function setCurrentEffect(num) {
    if (effect) {
        effect.disable();
        stopDraw();
    }
    currentEffect = num;
    effect = effects[num];
    effect.enable();
    beginDraw();
}

function stopDraw() {
    if (timer) {
        cancelAnimationFrame(timer);
    }
    isStopped = true;
}

for (i = 1, len = arguments.length; i < len; i++) {
    effects[i - 1] = arguments[i];
    effectListHtml += '<li><img  num="' + (i - 1) + '" src="img/' + effects[i - 1].cover + '"></li>';
}
$effectList.html(effectListHtml);
$effectList.on('click', 'li img', function () {
    setCurrentEffect($(this).attr('num'));
    util.setBg(currentEffect);
});

function draw() {
    if (isStopped != true) {
        cancelAnimationFrame(timer);
        timer = requestAnimationFrame(draw);
    }
    if (effect.isInit() === false) {
        effect.init();
    } else {
        effect.draw();
    }
}

function next() {
    currentEffect ++;
    currentEffect = currentEffect % effects.length;
    setCurrentEffect(currentEffect);
    util.setBg(currentEffect);
}

function pre() {
    currentEffect--;
    if (currentEffect == -1) {
        currentEffect = effects.length - 1;
    }
    setCurrentEffect(currentEffect);
    util.setBg(currentEffect);
}

function windowResize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

windowResize();
$(window).on('resize', function() {
    windowResize();
});

return {
    beginDraw: beginDraw,
    next: next,
    pre: pre,
    saveSetting: saveSetting,
    stopDraw: stopDraw
}

});
