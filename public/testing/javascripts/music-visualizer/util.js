define(function () {

var $background = $('#background'),
    $canvas = $('#visual-canvas'),
    doc = document;

function getById(id) {
    return doc.getElementById(id);
}

function getRidOfExtention(str) {
    return str.replace(/\.[0-9a-z]+$/i, '');
}

function intRandom(low, up) {
    return Math.floor(Math.random() * (up - low) + low);
}

function fullscreenSwitch() {
    var element = doc.documentElement;
    if (doc.fullscreenElement || doc.webkitFullscreenElement) {
        if(doc.cancelFullScreen) {
            doc.cancelFullScreen();
        } else if(doc.webkitCancelFullScreen) {
            doc.webkitCancelFullScreen();
        }
    } else {
        if (element.requestFullScreen) {
            element.requestFullScreen();
        } else if(element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        }
    }
}

function setBg(num) {
    $background.removeClass();
    $background.addClass('bg' + num);
}

function showCanvas() {
    $canvas.removeClass('hidden');
}

function hideCanvas() {
    $canvas.addClass('hidden');
}

return {
    getById: getById,
    getRidOfExtention: getRidOfExtention,
    intRandom: intRandom,
    fullscreenSwitch: fullscreenSwitch,
    setBg: setBg,
    showCanvas: showCanvas,
    hideCanvas: hideCanvas
}

});
