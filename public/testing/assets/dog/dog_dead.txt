.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-Dead-_01_ {
    width: 580px;
    height: 510px;
    background-position: -5px -5px;
}

.sprite-Dead-_02_ {
    width: 580px;
    height: 510px;
    background-position: -595px -5px;
}

.sprite-Dead-_03_ {
    width: 580px;
    height: 510px;
    background-position: -1185px -5px;
}

.sprite-Dead-_04_ {
    width: 580px;
    height: 510px;
    background-position: -1775px -5px;
}

.sprite-Dead-_05_ {
    width: 580px;
    height: 510px;
    background-position: -2365px -5px;
}

.sprite-Dead-_06_ {
    width: 580px;
    height: 510px;
    background-position: -2955px -5px;
}

.sprite-Dead-_07_ {
    width: 580px;
    height: 510px;
    background-position: -3545px -5px;
}

.sprite-Dead-_08_ {
    width: 580px;
    height: 510px;
    background-position: -4135px -5px;
}

.sprite-Dead-_09_ {
    width: 580px;
    height: 510px;
    background-position: -4725px -5px;
}

.sprite-Dead-_10_ {
    width: 580px;
    height: 510px;
    background-position: -5315px -5px;
}
