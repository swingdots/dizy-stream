.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-Idle-_01_ {
    width: 547px;
    height: 481px;
    background-position: -5px -5px;
}

.sprite-Idle-_02_ {
    width: 547px;
    height: 481px;
    background-position: -562px -5px;
}

.sprite-Idle-_03_ {
    width: 547px;
    height: 481px;
    background-position: -1119px -5px;
}

.sprite-Idle-_04_ {
    width: 547px;
    height: 481px;
    background-position: -1676px -5px;
}

.sprite-Idle-_05_ {
    width: 547px;
    height: 481px;
    background-position: -2233px -5px;
}

.sprite-Idle-_06_ {
    width: 547px;
    height: 481px;
    background-position: -2790px -5px;
}

.sprite-Idle-_07_ {
    width: 547px;
    height: 481px;
    background-position: -3347px -5px;
}

.sprite-Idle-_08_ {
    width: 547px;
    height: 481px;
    background-position: -3904px -5px;
}

.sprite-Idle-_09_ {
    width: 547px;
    height: 481px;
    background-position: -4461px -5px;
}

.sprite-Idle-_10_ {
    width: 547px;
    height: 481px;
    background-position: -5018px -5px;
}
