import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/main/home/home.component'
import { CanvasComponent } from './components/canvas/canvas.component'
import { AboutComponent } from './components/main/about/about.component'
import { MainComponent } from './components/main/main.component'


const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'canvas',
    component: CanvasComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  // {
  //   path: '**',
  //   redirectTo: ''
  // }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
