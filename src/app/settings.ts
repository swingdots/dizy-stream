export class Settings {
    
    public static webSocketPort = 8080;
    public static webSocketUri = 'stream'
    public static urlWordCount = '/api/chat/count';
    public static urlLastImage = '/api/upload/image';

    public static infoPanelMessage = "Play with Dizy";
    public static infoPanelMessageLeft = "DIZY.ST";
    public static infoPanelMessageRight = "";

    public static canvasPadding = 50
    public static canvasMaxWordBox = 20;
    public static canvasMaxWordCount = 5;
    public static canvasTimeTextLayout = 5000;
  
    public static listInfoPanelMessage = ['Terrible', 'Awful', 'Serious?', 'Not so good', 'So so', 'Not bad', 'Good', 'Great', 'Fantastic', 'Awesome' ];
    public static listInfoPanelColor = [ 'red', 'orangered', 'gold', 'yellowgreen', 'lime', 'green' ];

}
