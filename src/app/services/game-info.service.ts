import { Injectable, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebSocketService } from './web-socket.service'
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Settings } from '../settings'

export const OVERLAY_DATA = new InjectionToken<string>('OVERLAY_DATA');

@Injectable({
  providedIn: 'root'
})
export class GameInfoService {

  private listMessage = Settings.listInfoPanelMessage;
  private listColor = Settings.listInfoPanelColor;
  private urlWordCount = Settings.urlWordCount;
  private urlLastImage = Settings.urlLastImage;

  private subjectInfoPanelMessage = new BehaviorSubject('');
  public obsInfoPanelMessage = this.subjectInfoPanelMessage.asObservable();
  private subjectInfoPanelMessageLeft = new BehaviorSubject('');
  public obsInfoPanelMessageLeft = this.subjectInfoPanelMessageLeft.asObservable();
  private subjectInfoPanelMessageRight = new BehaviorSubject('');
  public obsInfoPanelMessageRight = this.subjectInfoPanelMessageRight.asObservable();
  // private subjectTextOverlayMessage = new BehaviorSubject('');
  // public obsTextOverlayMessage = this.subjectTextOverlayMessage.asObservable();
  // private subjectTextOverlayScore = new Subject<number>();
  // public obsTextOverlayScore = this.subjectTextOverlayScore.asObservable();
  
  private infoPanelMessage:string = Settings.infoPanelMessage;
  private infoPanelMessageLeft:string = Settings.infoPanelMessageLeft;
  private infoPanelMessageRight:string = Settings.infoPanelMessageRight;
  private textOverlayMessage: string;
  private textOverlayColor: string;

  get InfoPanelMessage(): string{
    return this.infoPanelMessage;
  }

  get InfoPanelMessageLeft(): string{
    return this.infoPanelMessageLeft;
  }

  get InfoPanelMessageRight(): string{
    return this.infoPanelMessageRight;
  }

  get TextOverlayMessage(): string{
    return this.textOverlayMessage;
  }
  get TextOverlayColor(): string{
    return this.textOverlayColor;
  }

  constructor(private http: HttpClient, private ws: WebSocketService) { }

  getWordCount(): Observable<any> {
    return this.http.get<any>(this.urlWordCount)
  }

  wsConnect() {
    var host = window.location.hostname;
    var url = `ws://${host}:${Settings.webSocketPort}/${Settings.webSocketUri}`;
    return this.ws.connect(url);
  }

  setInfoPanelMessage(message: string) {
    this.infoPanelMessage = message;
    this.subjectInfoPanelMessage.next(message);
  }

  setInfoPanelMessageLeft(message: string) {
    this.infoPanelMessageLeft = message;
    this.subjectInfoPanelMessageLeft.next(message);
  }

  setInfoPanelMessageRight(message: string) {
    this.infoPanelMessageRight = message;
    this.subjectInfoPanelMessageRight.next(message);
  }

  setTextOverlayMessage(message: string){
    this.textOverlayMessage = message.replace(/'|"/gi, '');
    this.textOverlayColor = 'orangered';
    // this.subjectTextOverlayMessage.next(message);
  }

  setTextOverlayScore(score: number){
    var indexMessage = Math.round(score * (this.listMessage .length - 1) );
    var indexColor = Math.round(score * (this.listColor.length - 1) );
    this.textOverlayMessage = this.listMessage[indexMessage];
    this.textOverlayColor = this.listColor[indexColor];
    // this.subjectTextOverlayScore.next(score);
  }


  getLastImage(): Observable<Blob>  {
    return this.http.get(this.urlLastImage, {responseType: 'blob'});
  }
}
