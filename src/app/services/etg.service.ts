import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EtgService {

  constructor() { }

  private subjectRoomDoors = new Subject<any>();
  public obsRoomDoors = this.subjectRoomDoors.asObservable();

  private listRoomDoors:any[];

  get ListRoomDoors(): any[]{
    return this.listRoomDoors;
  }

  setRoomDoors(obj: any){
    console.log('service setRoomDoors')
    this.listRoomDoors = obj;
    this.subjectRoomDoors.next(this.listRoomDoors);
  }

  setRoomDoorsSelected(index){
    this.listRoomDoors[index]["selected"] = true;
    this.subjectRoomDoors.next(this.listRoomDoors);
  }

}
