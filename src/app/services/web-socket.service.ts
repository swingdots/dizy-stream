import { Injectable } from '@angular/core';
import { Subject, Observer, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  constructor() { }

  private subject: Subject<MessageEvent>;

  public connect(url): Subject<MessageEvent> {
    this.subject = this.create(url);
    if (this.subject) {
        console.log("Successfully connected: " + url);
    }
    return this.subject;
  }

  private create(url): Subject<MessageEvent> {

      let ws = new WebSocket(url);
      // ws.onerror = (err) => {
      //   console.log(err);
      // };
      let observable = Observable.create(
        (obs: Observer<MessageEvent>) => {
          ws.onmessage = obs.next.bind(obs);
          ws.onerror = obs.error.bind(obs);
          ws.onclose = obs.complete.bind(obs);
          return ws.close.bind(ws);
        })
      let observer = {
        next: (data: any) => {
          if (ws.readyState === WebSocket.OPEN) {
            if(typeof(data) === "object"){
              ws.send(JSON.stringify(data));
            }
            else if(typeof(data) === "string"){
              ws.send(data);
            }
            else{
              ws.send(String(data));
            }
          }
        }
      }
      return Subject.create(observer, observable);
  }
}
