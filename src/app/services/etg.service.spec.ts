import { TestBed, inject } from '@angular/core/testing';

import { EtgService } from './etg.service';

describe('EtgService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EtgService]
    });
  });

  it('should be created', inject([EtgService], (service: EtgService) => {
    expect(service).toBeTruthy();
  }));
});
