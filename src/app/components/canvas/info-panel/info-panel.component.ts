import { Component, OnInit } from '@angular/core';
import { GameInfoService } from '../../../services/game-info.service'

@Component({
  selector: 'app-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.css']
})
export class InfoPanelComponent implements OnInit {

  constructor(private gameInfo: GameInfoService) { }

  message:string;
  messageLeft:string;
  messageRight:string;

  dateCurrent:Date;

  ngOnInit() {
    this.gameInfo.obsInfoPanelMessage.subscribe(message => this.updateMessage(message) );
    this.gameInfo.obsInfoPanelMessageLeft.subscribe(message => this.updateMessageLeft(message) );
    this.gameInfo.obsInfoPanelMessageRight.subscribe(message => this.updateMessageRight(message) );
    this.message = this.gameInfo.InfoPanelMessage;
    this.messageLeft = this.gameInfo.InfoPanelMessageLeft;
    this.messageRight = this.gameInfo.InfoPanelMessageRight;
    setInterval(() => { this.dateCurrent = new Date() }, 200);
  }

  filterMessage(message){
    return message.replace(/'|"/gi, '');
  }

  updateMessage(message){
    if(message.length > 0){
      this.message = this.filterMessage(message);
    }
  }

  updateMessageLeft(message){
    if(message.length > 0){
      this.messageLeft = this.filterMessage(message);
    }
  }

  updateMessageRight(message){
    if(message.length > 0){
      this.messageRight = this.filterMessage(message);
    }
  }

  public test(){
    console.log('test');
  }
}
