import { Component, OnInit, AfterViewInit, HostListener, ViewContainerRef, InjectionToken } from '@angular/core';
import { GameInfoService, OVERLAY_DATA } from '../../services/game-info.service'
import { Observable, Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Overlay, OverlayRef, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal, TemplatePortal, PortalInjector } from '@angular/cdk/portal';
import { InfoPanelComponent } from './info-panel/info-panel.component';
import { ImageOverlayComponent } from './image-overlay/image-overlay.component'
import { TextOverlayComponent } from './text-overlay/text-overlay.component'
import { Settings } from '../../settings'
import { EtgService } from '../../services/etg.service';


@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit, AfterViewInit {

  constructor(
    private gameInfoService: GameInfoService,
    private etgService: EtgService,
    public viewContainerRef: ViewContainerRef, public overlay: Overlay,
    public snackBar: MatSnackBar) {
  }

  padding = Settings.canvasPadding;
  maxWordBox = Settings.canvasMaxWordBox;
  maxWordCount = Settings.canvasMaxWordCount;
  timeTextLayout = Settings.canvasTimeTextLayout;

  messages: Subject<any>;
  listBox = [];
  listCount = [];
  width = 0;
  height = 0;
  isMOS = false;
  isWC = false;
  timeoutMOS = undefined;
  
  isInfoPanel: boolean = false;
  isImageOverlay: boolean = false;
  isTextOverlay: boolean = false;

  infoPanelRef: OverlayRef;
  imageOverlayRef: OverlayRef;
  textOverlayRef: OverlayRef;

  textOverlayMessage: String;

  ngOnInit() {
    this.onResize();
    this.connect();
    this.loadWordCout();
    this.openInfoPanel();
  }

  ngAfterViewInit() {

  }

  @HostListener('window:resize', ['$event.target'])
  onResize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }

  openTextOverlay(isTextOverlay?) {
    if( isTextOverlay !== undefined && isTextOverlay == this.isTextOverlay){
      return;
    }

    if(this.isTextOverlay){
      this.isTextOverlay = false;
      this.textOverlayRef.dispose();
      return;
    }

    this.isTextOverlay = true;
    let config = new OverlayConfig();
    config.positionStrategy = this.overlay.position()
        .global()
        .centerHorizontally()
        .centerVertically();

    this.textOverlayRef = this.overlay.create(config);
    this.textOverlayRef.attach(new ComponentPortal(TextOverlayComponent, this.viewContainerRef));
    
  }

  openInfoPanel(isText?) {
    if( isText !== undefined && isText == this.isInfoPanel){
      return;
    }

    if(this.isInfoPanel){
      this.isInfoPanel = false;
      this.infoPanelRef.dispose();
      return;
    }

    this.isInfoPanel = true;
    let config = new OverlayConfig();
    config.positionStrategy = this.overlay.position()
        .global()
        .centerHorizontally()
        .top('0px');

    this.infoPanelRef = this.overlay.create(config);
    this.infoPanelRef.attach(new ComponentPortal(InfoPanelComponent, this.viewContainerRef));

  }

  openImageOverlay(isImageOverlay?) {
    if( isImageOverlay !== undefined && isImageOverlay == this.isImageOverlay){
      return;
    }

    if(this.isImageOverlay){
      this.isImageOverlay = false;
      this.imageOverlayRef.dispose();
      return;
    }

    this.isImageOverlay = true;
    let config = new OverlayConfig();
    config.positionStrategy = this.overlay.position()
        .global()
        .centerHorizontally()
        .centerVertically();
    config.hasBackdrop = true;

    this.imageOverlayRef = this.overlay.create(config);
    this.imageOverlayRef.backdropClick().subscribe(() => {
      this.imageOverlayRef.dispose();
    });
    // this.imageOverlayRef.attach(new ComponentPortal(ImageOverlayComponent, this.viewContainerRef));
    // [TODO] PortalInjector is not working..........
    const injectionTokens = new WeakMap<any, any>([[OVERLAY_DATA, 'etg']]);
    var cp = new ComponentPortal(ImageOverlayComponent, this.viewContainerRef, new PortalInjector(this.viewContainerRef.injector, injectionTokens));
    this.imageOverlayRef.attach(cp);
  }

  openSnackBar(message) {
    this.snackBar.open(message, undefined, { duration: 3000 });
  }

  messageOnScreen(time){
    this.isMOS = true;
    if(time < 0){
      return;
    }

    if(this.timeoutMOS){
      clearTimeout(this.timeoutMOS);
      this.timeoutMOS = undefined;
    }
    this.timeoutMOS = setTimeout(() => {
      this.listBox = [];
      this.isMOS = false;
      // console.log('clear');
    }, time*1000);
  }

  displayTextOverlay(text: string, time: number){
    this.gameInfoService.setTextOverlayMessage(text);
    this.openTextOverlay(true);
    setTimeout(() => this.openTextOverlay(false) ,time)
  }

  displayTextOverlayByScore(score: number, time: number){
    this.gameInfoService.setTextOverlayScore(score);
    this.openTextOverlay(true);
    setTimeout(() => this.openTextOverlay(false) ,time)
  }

  handleCommand(cmd: String) {
    // var splt = cmd.split(/\s{1,}/).filter(String);
    console.log(cmd);

    if (cmd[0] == '!cont.vis') {
      switch (cmd[1]) {
        case 'mos':
          if (cmd.length > 2) {
            var t = Number.parseInt(cmd[2]);
            if (t !== NaN) {
              this.messageOnScreen(t);
            }
            this.openSnackBar("Received command: " + cmd);
          }
          break;
        case 'wc':
          if (cmd.length > 2) {
            var t = Number.parseInt(cmd[2]);
            if (t !== NaN) {
              this.isWC = t != 0;
            }
          }
          break;
        case 'info-msg':
          if (cmd.length > 2) {
            this.gameInfoService.setInfoPanelMessage(cmd[2]);
          }
          break;
        case 'info-msg-left':
          if (cmd.length > 2) {
            this.gameInfoService.setInfoPanelMessageLeft(cmd[2]);
          }
          break;
        case 'info-msg-right':
          if (cmd.length > 2) {
            this.gameInfoService.setInfoPanelMessageRight(cmd[2]);
          }
          break;
        case 'info-panel':
          if (cmd.length > 2) {
            var t = Number.parseInt(cmd[2]);
            if (t !== NaN) {
              this.openInfoPanel(t != 0);
            }
          }
          break;
        case 'text':
          if (cmd.length > 2) {
            this.displayTextOverlay(cmd[2], this.timeTextLayout);
          }
          else{
            this.openTextOverlay(false);
          }
          break;
        case 'image':
          if (cmd.length > 2) {
            var t = Number.parseInt(cmd[2]);
            if (t !== NaN) {
              this.openImageOverlay(t != 0);
            }
          }
          break;
        case 'etg-door':
          if (cmd.length > 2) {
            try {
              var data = JSON.parse(cmd[2].slice(1, cmd[2].length-1));
              console.log(data);
            }
            catch (e) {
              console.log(e);
            }
          }
          break;
        }
      // this.openSnackBar("Received command: " + cmd);
    }
  }

  handleWordCount(wcs) {
    for (var i = 0; i < this.maxWordCount; i++) {
      if (this.listCount.length <= i) {
        this.listCount.push(wcs[i])
      }
      else if (this.listCount[i].word != wcs[i].word
        || this.listCount[i].count != wcs[i].count) {
        // this.listCount.splice(i, 1, wcs[i]);
        this.listCount[i] = wcs[i];
      }
    }
  }

  onMessage(msg){
    try {
      
      var data = JSON.parse(msg.data);
      // console.log(data);
      // from message queue
      if(data.topic == 'play' || data.topic == 'chat'){
        switch(data.state){
          case 'map':
            // this.gameInfoService.setEtgDoors(data.location)
            this.etgService.setRoomDoors(data.location);
            this.openImageOverlay(true);
            // setTimeout(()=>this.gameInfoService.setEtgDoors(data.location), 1000);
            break;
          case 'destination':
            // console.log(data);
            // this.gameInfoService.setEtgDoorsSelected(data.dest);
            this.etgService.setRoomDoorsSelected(data.dest);
            setTimeout(()=> this.openImageOverlay(false), 2000);
            break;
          case 'dead':
            console.log(data);
            this.displayTextOverlayByScore(data.score, this.timeTextLayout);
            break;
        }
      }

      // from websocket
      if (data.type == 'chat') {
        this.generateWordBox(data.message);
      }
      else if (data.type == 'chat/count') {
        // console.log(data);
        this.handleWordCount(data.message);
      }
      else if (data.type == 'cmd') {
        // console.log(data);
        this.handleCommand(data.cmd);
      }
    }
    catch (e) {
      console.log(e);
    }

  }

  onError(err){
    console.log(err);
    setTimeout(this.connect, 3000);
  }

  onComplete(){
    console.log('websocket closed. try reconnect in 3 sec.');
    setTimeout(this.connect, 3000);
  }

  connect() {
    this.messages = this.gameInfoService.wsConnect();
    if (this.messages) {
      this.messages.subscribe(
        (msg) => this.onMessage(msg),
        (err) => this.onError,
        () => this.onComplete
      );
    }
    else{
      setTimeout(this.connect, 3000);
    }
  }

  loadWordCout = () => {
    this.gameInfoService.getWordCount().subscribe({
      next: x => {
        this.listCount = x.data;
      },
      error: err => {
        console.error('Observer got an error: ' + err);
      }
    });
  }


  generateWordBox = (value) => {
    if (this.isMOS == false){
      return;
    }
    var x = Math.floor(Math.random() * (this.width - this.padding * 2));
    var y = Math.floor(Math.random() * (this.height - this.padding * 2));
    var tf = `translate(${x + this.padding}px, ${y + this.padding}px)`;
    this.listBox.push({ "word": value, style: { 'transform': tf } });
    if (this.listBox.length > this.maxWordBox) {
      this.listBox.shift();
    }
  }


}
