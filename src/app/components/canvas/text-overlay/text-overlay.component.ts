import { Component, OnInit } from '@angular/core';
import { GameInfoService } from '../../../services/game-info.service'


@Component({
  selector: 'app-text-overlay',
  templateUrl: './text-overlay.component.html',
  styleUrls: ['./text-overlay.component.css']
})

export class TextOverlayComponent implements OnInit {

  constructor(private gameInfo: GameInfoService) { }

  message: string;
  messageColor: string;

  ngOnInit() {
    // this.gameInfo.obsTextOverlayMessage.subscribe(message => this.updateMessage(message) );
    // this.gameInfo.obsTextOverlayScore.subscribe(score => this.updateScore(score) );
    this.message = this.gameInfo.TextOverlayMessage;
    this.messageColor = this.gameInfo.TextOverlayColor;
  }

  // updateMessage(message){
  //   if(message.length > 0){
  //     this.messageColor = 'orangered';
  //     this.message = message.replace(/'|"/gi, '');
  //   }
  // }

  // updateScore(score){
  //   console.log("!!!!!!");
  //   var indexMessage = Math.floor(score * (list_message.length - 1) );
  //   var indexColor = Math.floor(score * (list_color.length - 1) );
  //   this.message = list_message[indexMessage];
  //   this.messageColor = list_color[indexColor];
  // }

}
