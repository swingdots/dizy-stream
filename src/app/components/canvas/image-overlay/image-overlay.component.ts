import { Component, OnInit, Inject } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { GameInfoService, OVERLAY_DATA } from '../../../services/game-info.service'
import { EtgService } from '../../../services/etg.service';


@Component({
  selector: 'app-image-overlay',
  templateUrl: './image-overlay.component.html',
  styleUrls: ['./image-overlay.component.css']
})
export class ImageOverlayComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer,
    private gameInfoService: GameInfoService,
    private etgService: EtgService) { }

  @Inject(OVERLAY_DATA) public ctid: string
  // @Input() contentId: string;
  contentId: string = 'etg';

  image: SafeUrl = "//:0";
  listRoundingRect: any[] = [];
  sizeRoundingRect = 40;
  scale = 0;
  imageWidth = 600;


  ngOnInit() {
    console.log('!!!!!!!!!!', this.ctid);
    // for etg
    switch (this.contentId) {
      case 'etg':
        this.etgService.obsRoomDoors.subscribe((msg) => { this.updateLayout() });
        break;
    }

    this.updateLayout();
  }

  updateRoomDoors(locations) {
    this.listRoundingRect = [];
    var offset = this.sizeRoundingRect / 2;
    locations.forEach((loc, index, arr) => {
      var x = loc.x * this.scale - offset;
      var y = loc.y * this.scale - offset;
      var tf = `translate(${x}px, ${y}px)`;
      var color = `${loc.selected ? 'darkred' : 'darkgreen'}`;
      var background = `${loc.selected ? 'lightsalmon' : '#e0ffff88'}`;
      var style = {
        'transform': tf, 'color': color, 'background-color': background,
        'width': `${this.sizeRoundingRect}px`, 'line-height': `${this.sizeRoundingRect}px`
      };
      this.listRoundingRect.push({ 'text': index, 'style': style });
    });
  }

  updateLayout() {
    this.gameInfoService.getLastImage().subscribe({
      next: x => {
        let urlCreator = window.URL;
        let objUrl = urlCreator.createObjectURL(x);
        this.image = this.sanitizer.bypassSecurityTrustUrl(objUrl);
        var img = new Image();
        img.onload = () => {
          this.scale = this.imageWidth / img.width;
          this.updateLayoutForContent();
        };
        img.src = objUrl;
      },
      error: err => {
        console.error('Observer got an error: ' + err);
      }
    });
  }

  updateLayoutForContent(){
    switch(this.contentId){
      case 'etg':
        this.updateRoomDoors(this.etgService.ListRoomDoors);
        break;
    }
  }


}
