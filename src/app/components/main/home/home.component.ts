import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  urlsAPIs = [
    { method: "GET", url: "/api/chat", text: "/api/chat" },
    { method: "GET", url: "/api/chat/count", text: "/api/chat/count" },
    { method: "GET", url: "/api/upload/image", text: "/api/upload/image" },
    { method: "POST", url: "/api/upload/image", text: "/api/upload/image" },
  ];

  urlsAngular = [
    { method: "GET", url: "/canvas", text: "/canvas" }
  ];

  urlsPublic = [
    { method: "GET", url: "/public/chat-renderer.html", text: "/public/chat-renderer.html" },
    { method: "GET", url: "/public/particle.html", text: "/public/particle.html" },
    { method: "GET", url: "/public/clock.html", text: "/public/clock.html" },
    { method: "GET", url: "/public/sprites.html", text: "/public/sprites.html" }
  ];

  ngOnInit() {
  }

}
