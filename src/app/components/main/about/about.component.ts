import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  contents = [];

  ngOnInit() {
    for(var i=0; i<1000; i++){
      this.contents.push("About component.");
    }
  }

}
