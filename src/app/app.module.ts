import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCheckboxModule, MatCardModule, MatToolbarModule, MatSidenavModule, MatListModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';

import { AppComponent } from './app.component';
import { AboutComponent } from './components/main/about/about.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './components/main/home/home.component';
import { CanvasComponent } from './components/canvas/canvas.component';
import { MainComponent } from './components/main/main.component';
import { InfoPanelComponent } from './components/canvas/info-panel/info-panel.component';
import { ImageOverlayComponent } from './components/canvas/image-overlay/image-overlay.component';
import { TextOverlayComponent } from './components/canvas/text-overlay/text-overlay.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    CanvasComponent,
    MainComponent,
    InfoPanelComponent,
    ImageOverlayComponent,
    TextOverlayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatBadgeModule,
    MatSnackBarModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    InfoPanelComponent,
    ImageOverlayComponent,
    TextOverlayComponent
  ]
})
export class AppModule { }
