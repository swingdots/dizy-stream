import json
import jsonschema
import datetime
import pprint
import json


class JsonHandler:
    def __init__(self, path2json='../server/json-schema/'):
        self.admin_game_filename = 'admin.game.json'
        self.admin_server_filename = 'admin.server.json'
        self.admin_log_filename = 'admin.log.json'
        self.channel_chat_filename = 'channel.chat.json'
        self.channel_donation_filename = 'channel.donation.json'
        self.channel_subscribe_filename = 'channel.subscribe.json'
        self.game_hs_filename = 'game.hs.json'
        self.log_filename = 'log.json'
        self.session_multiple_choice_filename = 'session.multiple_choice.json'
        self.session_short_answer_filename = 'session.short_answer.json'
        self.path2json = path2json
        self.load_schema()


    def load_schema(self):
        self.admin_game_schema = json.loads(open(self.path2json + self.admin_game_filename).read())
        self.admin_server_schema = json.loads(open(self.path2json + self.admin_server_filename).read())
        self.admin_log_schema = json.loads(open(self.path2json + self.admin_log_filename).read())
        self.channel_chat_schema = json.loads(open(self.path2json + self.channel_chat_filename).read())
        self.channel_donation_schema = json.loads(open(self.path2json + self.channel_donation_filename).read())
        self.channel_subscribe_schema = json.loads(open(self.path2json + self.channel_subscribe_filename).read())
        self.game_hs_schema = json.loads(open(self.path2json + self.game_hs_filename).read())
        self.log_schema = json.loads(open(self.path2json + self.log_filename).read())
        self.session_multiple_choice_schema = json.loads(open(self.path2json + self.session_multiple_choice_filename).read())
        self.session_short_answer_schema = json.loads(open(self.path2json + self.session_short_answer_filename).read())



    # {
    #     "description":"class:admin topic:game - to control game module",
    #     "type":"object",
    #     "required":["class", "topic", "cmd", "datetime"],
    #     "properties":{
    #         "class":{"const":"admin"},
    #         "topic":{"const":"game"},
    #         "cmd":{"enum":["load_model", "load_game", "load_config", "set_action", "shutdown", "broadcast", "reset"]},
    #         "option":{"type" : "object"},
    #         "datetime":{"format" : "date-time"}
    #     }
    # }
    def get_admin_game(self):
        json_obj = {
            'class':'admin',
            'topic':'game',
            'cmd':'load_config',
            'option':{},
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:admin topic:server - to control server module",
    #     "type":"object",
    #     "required":["class", "topic", "cmd", "datetime"],
    #     "properties":{
    #         "class":{"const":"admin"},
    #         "topic":{"const":"server"},
    #         "cmd":{"enum":["load_config", "shutdown", "broadcast", "reset"]},
    #         "option":{"type" : "object"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_admin_server(self):
        json_obj = {
            'class':'admin',
            'topic':'server',
            'cmd':'load_config',
            'option':{},
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:admin topic:log - to control or set up logging level",
    #     "type":"object",
    #     "required":["class", "topic", "level", "datetime"],
    #     "properties":{
    #         "class":{"const":"admin"},
    #         "topic":{"const":"log"},
    #         "level":{"enum":["error", "info", "debug"]},
    #         "datetime":{"format" : "date-time"}
    #     }
    # }
    def get_admin_log(self):
        json_obj = {
            'class':'admin',
            'topic':'log',
            'level':'info',
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:channel topic:chat - chat or comment event in channel",
    #     "type":"object",
    #     "required":["class", "topic", "msg", "platform", "datetime"],
    #     "properties":{
    #         "class":{"const":"channel"},
    #         "topic":{"const":"chat"},
    #         "direction":{"enum":["in", "out"]},
    #         "channel":{"type" : "string"},
    #         "user":{"type" : "string"},
    #         "msg":{"type" : "string"},
    #         "platform":{"enum":["twitch", "youtube", "twitter"]},
    #         "viewer_count":{"type":"integer"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_channel_chat(self):
        json_obj = {
            'class':'channel',
            'topic':'chat',
            'direction':'out',
            'channel':'',
            'user':'',
            'msg':'',
            'platform':'twitch',
            'viewer_count':0,
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:channel topic:donation - donation event in channel",
    #     "type":"object",
    #     "required":["class", "topic", "user", "msg", "amount", "unit", "platform", "datetime"],
    #     "properties":{
    #         "class":{"const":"channel"},
    #         "topic":{"const":"donation"},
    #         "channel":{"type":"string"},
    #         "user":{"type":"string"},
    #         "msg":{"type":"string"},
    #         "amount":{"type":"number"},
    #         "unit":{"type":"string"},
    #         "platform":{"enum":["twitch", "youtube", "twitter"]},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_channel_donation(self):
        json_obj = {
            'class':'channel',
            'topic':'donation',
            'channel':'',
            'user':'',
            'msg':'',
            'amount':0,
            'unit':'won',
            'platform':'twitch',
            'datetime':self.get_utc_now()
        }
        return json_obj


    # {
    #     "description":"class:channel topic:subscribe - subscribe event in channel",
    #     "type":"object",
    #     "required":["class", "topic", "channel", "user", "platform", "datetime"],
    #     "properties":{
    #         "class":{"const":"channel"},
    #         "topic":{"const":"subscribe"},
    #         "channel":{"type":"string"},
    #         "user":{"type":"string"},
    #         "subscribe_count":{"type":"integer"},
    #         "platform":{"enum":["twitch", "youtube", "twitter"]},
    #         "datetime":{"format" : "date-time"}
    #     }
    # }
    def get_channel_subscribe(self):
        json_obj = {
            'class':'channel',
            'topic':'subscribe',
            'channel':'',
            'user':'',
            'subsribe_count':0,
            'platform':'twitch',
            'datetime':self.get_utc_now()
        }
        return json_obj


    # {
    #     "description" : "class:game topic:hc - for hearthstone game",
    #     "type":"object",
    #     "required":["class", "topic", "datetime"],
    #     "properties":{
    #         "class":{"const": "game"},
    #         "topic":{"const" : "hs"},
    #         "my_class":{"enum":["druid", "hunter", "mage", "paladin", "priest", "rogue", "shaman", "warlock", "warrior"]},
    #         "opponent_class":{"enum":["druid", "hunter", "mage", "paladin", "priest", "rogue", "shaman", "warlock", "warrior"]},
    #         "my_cards":{"type":"array","items":{"type":"string"}},
    #         "opponent_cards":{"type":"array","items":{"type":"string"}},
    #         "turn":{"type":"integer"},
    #         "my_minions":{"type":"array","items":{"type":"string"}},
    #         "opponent_minions":{"type":"array","items":{"type":"string"}},
    #         "status":{"enum":["win", "loss", "playing", "standby"]},
    #         "win_probability":{"type" : "number", "minimum" : 0, "maximum" : 1.0},
    #         "cmd":{"enum":[ "play_on", "set_action"]},
    #         "option":{"type":"object"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_game_hs(self):
        json_obj = {
            'class':'game',
            'topic':'hs',
            'my_class':'druid',
            'opponent_class':'druid',
            'my_cards':[],
            'opponent_cards':[],
            'turn':0,
            'my_minions':[],
            'opponent_minions':[],
            'status':'standby',
            'win_probability':0,
            'cmd':'play_on',
            'option':{},
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:admin topic:log - to propagate logging information",
    #     "type":"object",
    #     "required":["class", "topic", "level", "origin", "msg", "datetime"],
    #     "properties":{
    #         "class":{"const":"log"},
    #         "topic":{"const":"log"},
    #         "level":{"enum":["error", "info", "debug"]},
    #         "origin":{"enum":["game", "server"]},
    #         "msg":{"type":"string"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_log(self):
        json_obj = {
            'class':'log',
            'topic':'log',
            'level':['info'],
            'origin':'',
            'msg':'',
            'datetime':self.get_utc_now()
        }
        return json_obj



    # {
    #     "description":"class:session topic:multiple_choice",
    #     "type":"object",
    #     "required":["class", "topic", "options", "status", "datetime"],
    #     "properties":{
    #         "class":{"const":"session"},
    #         "topic":{"const":"multiple_choice"},
    #         "options":{"type":"array", "items": {"type" : ["string", "number"]}},
    #         "channel":{"type":"string"},
    #         "option_type":{"enum":["string", "integer", "number"]},
    #         "messages":{
    #             "type":"array",
    #             "items":{
    #                 "type":"object",
    #                 "properties":{"user":{"type":"string"}, "msg":{"type":["string", "number"]}}}},
    #         "result":{"type":"string"},
    #         "owner":{"type":"string"},
    #         "status":{"enum":["on", "off", "cancel", "request"]},
    #         "duration":{"type":"integer"},
    #         "num_response":{"type":"integer"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_session_multiple_choice(self):
        json_obj = {
            'class':'session',
            'topic':'multiple_choice',
            'options':['a'],
            'channel':'',
            'option_type':'string',
            'messages':[],
            'result':'',
            'owner':'gamer',
            'status':'standby',
            'duration':0,
            'num_response':0,
            'datetime':self.get_utc_now()
        }
        return json_obj

    # {
    #     "description":"class:session topic:short_answer",
    #     "type":"object",
    #     "required":["class", "topic", "options", "status", "datetime"],
    #     "properties":{
    #         "class":{"const":"session"},
    #         "topic":{"const":"short_answer"},
    #         "channel":{"type":"string"},
    #         "messages":{
    #             "type":"array", 
    #             "items":{
    #                 "type":"object",
    #                 "properties":{"user":{"type":"string"}, "msg":{"type":["string", "number"]}}}},
    #         "result":{"type":["string", "number"]},
    #         "owner":{"type":"string"},
    #         "status":{"enum":["on", "off", "cancel", "request"]},
    #         "duration":{"type":"integer"},
    #         "num_response": {"type": "integer"},
    #         "datetime":{"format":"date-time"}
    #     }
    # }
    def get_session_short_answer(self):
        json_obj = {
            'class':'session',
            'topic':'short_answer',
            'channel':'',
            'messages':[],
            'result':'',
            'owner':'gamer',
            'status':'standby',
            'duration':0,
            'num_response':0,
            'datetime':self.get_utc_now()
        }
        return json_obj


    def validate_admin_server(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.admin_server_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_admin_game(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.admin_game_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_admin_log(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.admin_log_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_channel_chat(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.channel_chat_schema)
            valid = True
        except Exception:
            valid = False
        return valid
    
    
    def validate_channel_donation(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.channel_donation_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_channel_subscribe(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.channel_subscribe_schema)
            valid = True
        except Exception:
            valid = False
        return valid          


    def validate_game_hs(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.game_hs_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_log(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.log_schema)
            valid = True
        except Exception:
            valid = False
        return valid


    def validate_session_multiple_choice(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.session_multiple_choice_schema)
            valid = True
        except Exception:
            print(Exception)
            valid = False
        return valid


    def validate_session_short_answer(self, data):
        valid = False
        try:
            jsonschema.validate(data, self.session_short_answer_schema)
            valid = True
        except Exception:
            print(Exception)
            valid = False
        return valid


    def get_utc_now(self):
        return datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    def toJsonString(self, data):
        return json.dumps(data, ensure_ascii=False).encode('utf8').decode("utf-8")

    def prettyprint(self, data):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(data)





def main():
    handler = JsonHandler()
    
    js = handler.get_admin_game()
    handler.prettyprint(js)
    print(handler.validate_admin_game(js))

    js = handler.get_admin_server()
    handler.prettyprint(js)
    print(handler.validate_admin_server(js))

    js = handler.get_admin_log()
    handler.prettyprint(js)
    print(handler.validate_admin_log(js))

    js = handler.get_channel_chat()
    handler.prettyprint(js)
    print(handler.validate_channel_chat(js))

    js = handler.get_channel_donation()
    handler.prettyprint(js)
    print(handler.validate_channel_donation(js))

    js = handler.get_channel_subscribe()
    handler.prettyprint(js)
    print(handler.validate_channel_subscribe(js))

    js = handler.get_game_hs()
    handler.prettyprint(js)
    print(handler.validate_game_hs(js))

    js = handler.get_log()
    handler.prettyprint(js)
    print(handler.validate_log(js))

    js = handler.get_session_multiple_choice()
    handler.prettyprint(js)
    print(handler.validate_session_multiple_choice(js))

    js = handler.get_session_short_answer()
    handler.prettyprint(js)
    print(handler.validate_session_short_answer(js))




if __name__ == "__main__":
    main()

