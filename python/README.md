# DizyStream/python

Python examples to work with dizystream web server.


## Requirements
* [jsonschema for python](https://github.com/Julian/jsonschema)
* [Erlang](http://www.erlang.org/)
* [RabbitMQ](https://www.rabbitmq.com/)


