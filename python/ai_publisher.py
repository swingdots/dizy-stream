import pika
import time
import logging
import json


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
                '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)


class AIPublisher():
    def __init__(self, url, exchange, exchange_type):
        self.url = url
        self.heartbeat_interval=60
        self.blocked_connection_timeout=300
        self.exchange = exchange
        self.exchange_type = exchange_type
        self._is_active = False
        self.init_conn()


    def init_conn(self):
        self.params = pika.ConnectionParameters(host=self.url,
                                                heartbeat_interval=self.heartbeat_interval,
                                                blocked_connection_timeout=self.blocked_connection_timeout)
        self.init_publisher()


    def init_publisher(self):
        self._connection = pika.BlockingConnection(self.params)
        # self._channel = self._connection.channel()
        # self._channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type)
        self._is_active = True


    ##########################################################################################
    # publishes msg to amqp
    #
    # topic_key : publish routing_key string
    # message : message in python dict type
    ##########################################################################################
    def publish(self, topic_key, message):
        self.init_conn()
        self._channel = self._connection.channel()
        self._channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type)
        message = self.toJsonString(message)
        self._channel.basic_publish(exchange=self.exchange,
                                    routing_key=topic_key,
                                    body=message)
        self.close()
        print(message)


    def close(self):
        if self._channel:
            self._channel.close()
        if self._connection:
            self._connection.close()
        self._is_active = False

    def get_status(self):
        return self._is_active

    def toJsonString(self, data):
        return json.dumps(data, ensure_ascii=False).encode('utf8').decode("utf-8")




if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    url = 'localhost'
    exchange = 'ai'
    exchange_type = 'topic'
    routing_keys = ['ai.info', 'hello.world']

    pub = AIPublisher(url, exchange, exchange_type)
    print("publisher ready : ", pub.get_status())
    time.sleep(2.0)


    for x in range(0, 5):
        msg = 'hey dizyhub whats up' + str(x)
        if x % 2 == 0:
            pub.publish(routing_keys[0],msg)
        else:
            pub.publish(routing_keys[1],msg)
        print("publishing : ", msg)
        time.sleep(0.5)
    pub.close()
    print("conn closed")

