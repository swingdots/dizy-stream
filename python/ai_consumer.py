from multiprocessing import Process, Queue
import pika
import time
import logging
import json


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
                '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)


class AIConsumer(Process):
    def __init__(self, url, exchange, routing_keys, exchange_type, q):
        super(AIConsumer, self).__init__()
        self.url = url
        self.heartbeat_interval=60
        self.blocked_connection_timeout=300
        self.exchange = exchange
        self.routing_keys = routing_keys
        self.exchange_type = exchange_type
        self._q = q
        self._is_active = False
        self._is_consuming = False


    def init_conn(self):
        self.params = pika.ConnectionParameters(host=self.url,
                                                heartbeat_interval=self.heartbeat_interval,
                                                blocked_connection_timeout=self.blocked_connection_timeout)
        self._connection = pika.BlockingConnection(self.params)
        self._channel = self._connection.channel()
        self.init_consume()


    def init_consume(self):
        self._channel.exchange_declare(exchange=self.exchange,
                                      exchange_type=self.exchange_type)
        self._result = self._channel.queue_declare(exclusive=True)

        for routing_key in self.routing_keys:
            self._channel.queue_bind(exchange=self.exchange,
                                    queue=self._result.method.queue,
                                    routing_key=routing_key)
        self._is_activce = True
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         self._result.method.queue,
                                                         no_ack=True)


    ##########################################################################################
    # consume message handler
    # constructs message as python dict type and puts into queue
    ##########################################################################################
    def on_message(self, channel, method, properties, body):
        body = body.decode('UTF-8')
        data = json.loads(body)
        print("{} : {} : '{}'".format(self.name, method.routing_key, body))
        self._q.put_nowait(data)


    def run(self):
        self.init_conn()
        self._channel.start_consuming()


    def stop(self):
        LOGGER.info('Stopping')
        self._closing = True
        self.stop_consuming()
        self.close()
        LOGGER.info('Stopped')


    def stop_consuming(self):
        self._channel.basic_cancel(consumer_tag=self._consumer_tag)
        self._is_consuming = False


    def close(self):
        if self._channel:
            self._channel.close()
        if self._connection:
            self._connection.close()
        self._is_active = False
        self._is_consuming = False





if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    host = 'localhost'
    exchange = 'ai'
    routing_keys = ['ai.info', '*.*']
    exchange_type = 'topic'
    queue = Queue()

    consumer = AIConsumer(host, exchange ,routing_keys, exchange_type, queue)
    consumer.start()


