const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();

const eventHandler = require("./server/event-bus");
// web socket
// let vs = { ws: null };
let list_ws = [];
const servers = require('./server/ws-server');
const wss = servers.stream;
wss.on('connection', function connection(ws) {
    console.log("WebSocket client connected...");
    list_ws.push(ws);
    ws.on('message', function incoming(message) {
        console.log('received: %s on server.js', message);
        ws.send("{message:'hello'}");
    });
    ws.on('close', function close() {
        console.log("WebSocket client disconnected...");
        index = list_ws.indexOf(ws);
        if (index >= 0) {
            list_ws.splice(index, 1);
        }
    });
});

// evnets for message queue
eventHandler.on('actor', function (msg) {
    list_ws.forEach((ws, index, arr) => {
        ws.send(JSON.stringify(msg));
    });
});

// events for chat
const onChat = (message) => {
    list_ws.forEach((ws, index, arr) => {
        ws.send(JSON.stringify(message));
    });
}

// events for word count
const onWordCount = (message) => {
    list_ws.forEach((ws, index, arr) => {
        ws.send(JSON.stringify({ type: 'chat/count', message: message }));
    });
}


// youtube chat
const yc = require('./server/youtube-chat');
yc.setOnChat(onChat);
yc.setOnWordCount(onWordCount);


// twitch chat
const tc = require('./server/twitch-chat');
tc.setOnChat(onChat);
tc.setOnWordCount(onWordCount);


// rabbit
const rabbit = require('./server/rabbit-dizy')
rabbit.start();

// session handler
const sh = require('./server/session-handler');
sh.init();


// twitter
const twitter = require('./server/twitter-api')
// [example code] get tweets from the timeline
twitter.timeline((err, tweets, res) => {
    if (err) {
        console.error(err[0]);
        return;
    }
    console.log("[Tweets]");
    tweets.forEach(element => {
        console.log("   - ", element.text)
    });
});
// // [example code] tweet status update and destroy that after 10 seconds.
// const status = 'hello dizy';
// twitter.update(status, (err, tweet, res) => {
//     if(err){
//         console.error(err[0]);
//         return;
//     }
//     const tid = tweet.id_str;
//     console.log("Tweet ID: ", tid);
//     setTimeout(() => {
//         twitter.destroy(tid, (err, tweet, res) => {
//             if(err) throw err;
//             console.log('Tweet destroied: ', tid);
//         });
//     }, 10000);
// });



// API file for interacting with MongoDB
const api = require('./server/routes/api');



// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// API location
app.use('/api', api);
app.use('/public', express.static('public'));
app.use('/', express.static('dist/dizy-stream'));
app.use('*', express.static('dist/dizy-stream'));

// app.use('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'dist/my-ng-canvas/index.html'));
// });


// // Send all other requests to the Angular app
// app.get('*', (req, res) => {
//     console.log(__dirname);
//     console.log(req);
//     res.sendFile(path.join(__dirname, 'dist/my-ng-canvas/index.html'));
// });

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));
